Master's Thesis: In thesis folder, use "make" command and "thesis-example.pdf" should popup.

Codes are in median_filter folder.
    To benchmark use "make" command to compile main.cc, ctmf8.c, ctmf8.h, ctmf16.c, and ctmf16.h files. The command produces an executeable file, median.
    
    To analyse raw data produced by executeable file, median, then run data_analysis.py
