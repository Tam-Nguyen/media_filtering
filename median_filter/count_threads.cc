#include <omp.h>
#include <iostream>

int main(int argc, char** argv)
{
  omp_set_dynamic(0);
  for(int i=1 ; i <= 8; i<<=1)
    {
      int sum = 0;
      omp_set_num_threads(i);
#pragma omp parallel
      {
        #pragma omp atomic
        sum++;
      }
      std::cout<< "i : " << i << ", sum: " << sum  << std::endl;
    }
  return 0;
}
