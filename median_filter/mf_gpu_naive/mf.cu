#include <stdlib.h>
#include "mf.h"
#include <iostream>
#include <algorithm>
#include <functional>
//#include <thrust/device_vector.h>
//#include <thrust/sort.h>
//#include <thrust/execution_policy.h>

using namespace std;

__global__ void cuda_mf(int ny, int nx, int hy, int hx, const int MAX_THREADS_BLOCK, const float* d_in, float* d_out) {
    // Define output coordinates
    int global_thread_id = blockIdx.x*MAX_THREADS_BLOCK+threadIdx.x;

    if(global_thread_id < ny*nx){
      int x = global_thread_id % nx;
      int y = global_thread_id / nx;
      //Defining window corners
      int x_min = max(0,x-hx);
      int x_max = min(nx-1,x+hx);
      int y_min = max(0,y-hy);
      int y_max = min(ny-1,y+hy);
      
      //push all pixel values of the sliding window into float vector
      int width = x_max-x_min+1;
      int height = y_max-y_min+1;
      float* d_v = (float*) malloc(sizeof(float)*width*height);
      
      //cudaMalloc((void**)&d_v,sizeof(float)*width*height);
      //thrust::device_vector<float> d_v(width*height);
      //vector<float> v;
      //d_v.reserve(width*height);
      int d_v_length = 0;
      for(int i=x_min;i<=x_max; i++){
	for(int j=y_min;j<=y_max; j++)
	  {
	    d_v[d_v_length] = d_in[i+nx*j];
	    d_v_length++;
	    //v.push_back(d_in[i+nx*j]);
	    //v.at(width*(j-y_min)+i-x_min)=in[i+nx*j]; //Why can't use v.at(0)?????
	  }
      }

      //qsort(d_v,d_v_length,sizeof(float),compare);

      // http://en.wikipedia.org/wiki/Insertion_sort
      for(int i=0; i<d_v_length; i++)
	{
	  float data = d_v[i];
	  int j = i;

	  while( j > 0 && d_v[j-1] > data){
	    d_v[j] = d_v[j-1];
	    j--;
	  }
	  d_v[j] = data;
	}
      //thrust::sort(thrust::seq,d_v,d_v+d_v_length);
      
      //Get the median using nth_element. Hinted.
      int avg = d_v_length/2;
      float median1 = d_v[avg];
      
      //if vector size is odd then median1 is middle
      int parity = d_v_length%2;
      switch(parity)
	{
	case 1:
	  d_out[x+nx*y] = median1;
	  break;
	default:
	  float median2 = d_v[avg-1];
	  d_out[x+nx*y] = 0.5*(median1 + median2);
	}
      free(d_v);
    }
}

void mf(int ny, int nx, int hy, int hx, const float* in, float* out){

    const int MAX_THREADS_BLOCK = 512;
    int remainder = (ny*nx) % MAX_THREADS_BLOCK;
    int NUM_BLOCKS = ny*nx/MAX_THREADS_BLOCK;
    if(remainder!=0 || NUM_BLOCKS==0) NUM_BLOCKS++;

    float* d_in;
    float* d_out;
    
    cudaMalloc((void**) &d_in, sizeof(float)*nx*ny);
    cudaMalloc((void**) &d_out, sizeof(float)*nx*ny);
    cudaMemcpy((void*) d_in, in,sizeof(float)*nx*ny,cudaMemcpyHostToDevice);
    
    cuda_mf<<<NUM_BLOCKS, MAX_THREADS_BLOCK>>>(ny,nx,hy,hx,MAX_THREADS_BLOCK,d_in,d_out);

    cudaMemcpy((void*)out,(void*) d_out,sizeof(float)*nx*ny,cudaMemcpyDeviceToHost);
    cudaFree(d_in);
    cudaFree(d_out);

}
