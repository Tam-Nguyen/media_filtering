#!/usr/bin/python3

#with plt plot only
# http://stackoverflow.com/questions/21254472/multiple-plot-in-one-figure-in-python
# example
# plt.plot(method_df["Resolution"].tolist(),method_df['Duration (ms)'],label='Naive',marker='+')


import matplotlib.pyplot as plt
import csv
import pandas
import sys
SEP = "|"
METHOD_COLUMN = "Method"
PIXEL_COLUMN = "Pixel depth"
RESOLUTION_COLUMN = "Resolution"
CORES_COLUMN = "Cores"
ITERATION_COLUMN = 'Iteration #'
DURATION_COLUMN = 'duration (ms)'
AVERAGE_DURATION = "Average " + DURATION_COLUMN
BITS_PER_BYTE = 8
METHODS = ["CTMF", "Naive", "Naive Improved", "Median Heap"]
METHODS_MAP={"CTMF":"CTMF", "Naive":"naive", "Improved Naive":"improved naive", "Median Heap":"median heap"}
COLORS = ["#ff0000", "#00ff00", "#0000ff", "#FFA500", "#A9A9A9","#D2B48C", "#D2691E", "#800080", "#CD853F", "#808000", "#000080", "#FF00FF"]
LINESTYLES = ['-', '--','-.',':']
MARKERS = ['o','s','*','^','v','|','x','H','p','v','<','>','D','+']

font = {'weight':'normal', 'size': 20}
#font2 = {'family' : 'monospace', 'weight':'normal', 'size': 5}

CACHES = {"L1 D/I Cache":32*1024/10**6,"L2 Cache":256*1024/10**6, "L3 Cache":15360*1024/10**6}
CACHES_COLORS = ["#DCDCDC", "#696969", "#2F4F4F"]

X_AXIS_PIXEL_TITLE = "Megapixels"

data_frame = pandas.read_csv(sys.argv[1],sep=SEP)
#get all columns names
ALL_COLUMNS = data_frame.columns.values.tolist()
#get means. ALL_COLUMNS[:-2] means all except two last columns.
mean_df = data_frame.groupby(ALL_COLUMNS[:-2], as_index=False).mean()
#drop the iteration column inplace.
mean_df.drop(ALL_COLUMNS[-2], axis=1, inplace=True)

# "Pixel depth|Method|Test Case|Resolution|Radius|Cores|Iteration #|Duration (ms)\n";
PIXEL_INDEX, METHOD_INDEX, TEST_CASE_INDEX, RESOLUTION_INDEX, RADIUS_INDEX, NTHREADS_INDEX, ITERATION_INDEX, DURATION_INDEX = range(8)

PIXEL_DEPTHS = sorted(mean_df[ALL_COLUMNS[PIXEL_INDEX]].drop_duplicates().tolist())
METHODS = sorted(mean_df[ALL_COLUMNS[METHOD_INDEX]].drop_duplicates().tolist())
TEST_CASES = sorted(mean_df[ALL_COLUMNS[TEST_CASE_INDEX]].drop_duplicates().tolist())
NTHREADS = sorted(mean_df[ALL_COLUMNS[NTHREADS_INDEX]].drop_duplicates().tolist())
RADIUS = sorted(mean_df[ALL_COLUMNS[RADIUS_INDEX]].drop_duplicates().tolist())
RESOLUTIONS = sorted(mean_df[ALL_COLUMNS[RESOLUTION_INDEX]].drop_duplicates().tolist())

MEGA = 10**6
X_LIM = [0.0, (data_frame[ALL_COLUMNS[RESOLUTION_INDEX]].max()+50)**2/MEGA]
Y_LIM = [0, 1000]
# +            leg = plt.legend(loc="upper left", shadow=True, fancybox=True)
# +            leg.get_frame().set_alpha(0.5)

# x bits, single threaded algorithms, starts
for pixel_bit in PIXEL_DEPTHS:
    for test_case in TEST_CASES:
        for index, method in enumerate(METHODS):
            df_single_thread = mean_df[(mean_df[ALL_COLUMNS[METHOD_INDEX]] == method)
                                       & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                       & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]==1)
                                       & (mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)]

            data_frame2 = data_frame[(data_frame[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                         & (data_frame[ALL_COLUMNS[METHOD_INDEX]]== method)
                                         & (data_frame[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                         & (data_frame[ALL_COLUMNS[NTHREADS_INDEX]]== 1)]
            minimum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).min()
            minimum = minimum[ALL_COLUMNS[DURATION_INDEX]].tolist()
            maximum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).max()
            maximum = maximum[ALL_COLUMNS[DURATION_INDEX]].tolist()
            
            mean = df_single_thread[ALL_COLUMNS[DURATION_INDEX]].tolist()
            maxmean = [x-y for x,y in zip(maximum, mean)]
            meanmin = [y-x for x,y in zip(minimum, mean)]
            plt.errorbar([x*x/MEGA for x in df_single_thread[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                         , df_single_thread[ALL_COLUMNS[DURATION_INDEX]].tolist()
                         , yerr=[meanmin, maxmean]
                         , color=COLORS[index]
                         , linestyle=LINESTYLES[0]
                         , linewidth=1.5
                         , marker=MARKERS[index]
                         , label= method)
            """
            plt.plot([x*x/MEGA for x in df_single_thread[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                     , df_single_thread[ALL_COLUMNS[DURATION_INDEX]].tolist()
                     , color=COLORS[index]
                     , linestyle=LINESTYLES[0]
                     , linewidth=1.5
                     , marker=MARKERS[index]
                     , label= method)
            """
        #for index,cache in enumerate(CACHES):
        #    plt.axvline(x=(CACHES[cache]*BITS_PER_BYTE/pixel_bit)**0.5,label=cache,color=CACHES_COLORS[index])

        plt.xlim(X_LIM)
        plt.title(str(pixel_bit) + "-bit images, single-threaded,\n"+test_case + " data",size=22,y=1.02)
        plt.ylabel(AVERAGE_DURATION, size=18)
        plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
        plt.legend(loc="upper left", shadow=True, fancybox=True, prop={"size":18})
        plt.grid()
        #set_fonts()
        #plt.style.use('presentation')
        plt.tick_params(labelsize=16)
        #plt.rc('font', **font)
        plt.savefig(str(pixel_bit)+"-bit_all_single_threaded_"+test_case+"_data.pdf",bbox_inches='tight',pad_inches = 0)
        plt.gcf().clear()
        #plt.xlabel(ALL_COLUMNS[RESOLUTION_INDEX])

# x bits, single threaded algorithms, ends
#

# x bits, parallized methods, starts
for pixel_bit in PIXEL_DEPTHS:
    for test_case in TEST_CASES:
        for method in METHODS:
            for index, nthreads in enumerate(NTHREADS):
                df_parallized_method = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                               & (mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                               & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                               & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)
                                               ]

                data_frame2 = data_frame[(data_frame[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                         & (data_frame[ALL_COLUMNS[METHOD_INDEX]]== method)
                                         & (data_frame[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                         & (data_frame[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)]
                minimum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).min()
                minimum = minimum[ALL_COLUMNS[DURATION_INDEX]].tolist()
                maximum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).max()
                maximum = maximum[ALL_COLUMNS[DURATION_INDEX]].tolist()
            
                mean = df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                maxmean = [x-y for x,y in zip(maximum, mean)]
                meanmin = [y-x for x,y in zip(minimum, mean)]
                plt.errorbar([x*x/MEGA for x in df_parallized_method[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                         , df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                         , yerr=[meanmin, maxmean]
                         , color=COLORS[index]
                         , linestyle=LINESTYLES[0]
                         , linewidth=1.5
                         , marker=MARKERS[index]
                         , label= (method + ", "+str(nthreads)+" thr."
                                   if method != METHODS[0]
                                   else str(nthreads)+" threads"))
                """
                plt.plot([x*x/MEGA for x in df_parallized_method[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                         , df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                         , color=COLORS[index]
                         , linestyle=LINESTYLES[0]
                         , linewidth=1.5
                         , marker=MARKERS[index]
                         , label= method + ", "+str(nthreads)+" threads")

                """
            if method != METHODS[0] and pixel_bit != PIXEL_DEPTHS[-1]:
            # Draw 8-bit CTMF single threaded if differ
                df_ctmf_single = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == PIXEL_DEPTHS[0])
                                         & (mean_df[ALL_COLUMNS[METHOD_INDEX]]== METHODS[0])
                                         & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                         & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== 1)]

                
                data_frame2 = data_frame[(data_frame[ALL_COLUMNS[PIXEL_INDEX]] == PIXEL_DEPTHS[0])
                                         & (data_frame[ALL_COLUMNS[METHOD_INDEX]]== METHODS[0])
                                         & (data_frame[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                         & (data_frame[ALL_COLUMNS[NTHREADS_INDEX]]== 1)]
                minimum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).min()
                minimum = minimum[ALL_COLUMNS[DURATION_INDEX]].tolist()
                maximum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).max()
                maximum = maximum[ALL_COLUMNS[DURATION_INDEX]].tolist()
                
                mean = df_ctmf_single[ALL_COLUMNS[DURATION_INDEX]].tolist()
                maxmean = [x-y for x,y in zip(maximum, mean)]
                meanmin = [y-x for x,y in zip(minimum, mean)]
                plt.errorbar([x*x/MEGA for x in df_ctmf_single[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                             , df_ctmf_single[ALL_COLUMNS[DURATION_INDEX]].tolist()
                             , yerr=[meanmin, maxmean]
                             , color=COLORS[index+1]
                             , linestyle=LINESTYLES[1]
                             , linewidth=1.5
                             , marker=MARKERS[index+1]
                             , label= METHODS[0] + ", 1 thread" )

                """
                plt.plot([x*x/MEGA for x in df_ctmf_single[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                         , df_ctmf_single[ALL_COLUMNS[DURATION_INDEX]].tolist()
                         , color=COLORS[index+1]
                         , linestyle=LINESTYLES[1]
                         , linewidth=1.5
                         , marker=MARKERS[index+1]
                         , label= METHODS[0] + ", single thread, "+ str(PIXEL_DEPTHS[0]) + "-bit" )
                """
            
            #Draw caches limit
            #for index,cache in enumerate(CACHES):
            #    plt.axvline(x=(CACHES[cache]*BITS_PER_BYTE/pixel_bit)**0.5,label=cache,color=CACHES_COLORS[index])

            plt.xlim(X_LIM)
            plt.title(str(pixel_bit) + "-bit images, multi-threaded " + METHODS_MAP[method] + ",\n" + test_case + " data",size=22, y=1.02)
            plt.ylabel(AVERAGE_DURATION, size=18)
            #plt.xlabel(RESOLUTION_COLUMN)
            plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
            plt.legend(loc="upper left", shadow=True, fancybox=True, prop={"size":15})
            plt.grid()
            plt.tick_params(labelsize=16)
            #plt.rc('font', **font)
            plt.savefig(str(pixel_bit)+"-bit_" + method.replace(" ","_") + "_parallized_"+test_case+ ".pdf",bbox_inches='tight',pad_inches = 0)
            plt.gcf().clear()
# x bits, parallized methods ends

# x bits, all fully parallized, starts
for pixel_bit in PIXEL_DEPTHS:
    for test_case in TEST_CASES:
        for index, method in enumerate(METHODS):
            nthreads = NTHREADS[-1]
            df_parallized_method = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                           &(mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                           & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                           & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)
                                       ]
            data_frame2 = data_frame[(data_frame[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                     & (data_frame[ALL_COLUMNS[METHOD_INDEX]]== method)
                                     & (data_frame[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                     & (data_frame[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)]
            minimum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).min()
            minimum = minimum[ALL_COLUMNS[DURATION_INDEX]].tolist()
            maximum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).max()
            maximum = maximum[ALL_COLUMNS[DURATION_INDEX]].tolist()
            
            mean = df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
            maxmean = [x-y for x,y in zip(maximum, mean)]
            meanmin = [y-x for x,y in zip(minimum, mean)]
            plt.errorbar([x*x/10**6 for x in df_parallized_method[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                     , df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                     , yerr=[meanmin, maxmean]
                     , color=COLORS[index]
                     , linestyle=LINESTYLES[0]
                     , linewidth=1.5
                     , marker=MARKERS[index]
                     , label= method + ", "+str(nthreads)+" threads")
            """
            plt.plot([x*x/10**6 for x in df_parallized_method[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                     , df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                     , color=COLORS[index]
                     , linestyle=LINESTYLES[0]
                     , linewidth=1.5
                     , marker=MARKERS[index]
                     , label= method + ", "+str(nthreads)+" threads")
            """
        #Draw caches limit
        #for index,cache in enumerate(CACHES):
        #    plt.axvline(x=(CACHES[cache]*BITS_PER_BYTE/pixel_bit)**0.5,label=cache,color=CACHES_COLORS[index])

        plt.xlim(X_LIM)
        plt.title(str(pixel_bit) + "-bit images, multi-threaded,\n"+test_case + " data", size=22,y=1.02)
        plt.ylabel(AVERAGE_DURATION, size=18)
        #plt.xlabel(RESOLUTION_COLUMN)
        plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
        plt.legend(loc="upper left", shadow=True, fancybox=True, prop={"size":15})
        plt.grid()
        plt.tick_params(labelsize=16)
        #plt.rc('font', **font)
        plt.savefig(str(pixel_bit)+"-bit_all_parallized_"+test_case+ "_data.pdf",bbox_inches='tight',pad_inches = 0)
        plt.gcf().clear()

# x bits, all fully parallized, ends

# 8 and 16 bits ctmf, all fully parallized at lower scale, starts
pixel_bit = PIXEL_DEPTHS[0]
method = METHODS[0]
limit = 2050
for pixel_bit in PIXEL_DEPTHS:
    for test_case in TEST_CASES:
        for index, nthreads in enumerate(NTHREADS):
            df_parallized_method = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                           &(mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                           & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                           & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)
                                       ]
            x_points =[x*x/10**6 for x in df_parallized_method[ALL_COLUMNS[RESOLUTION_INDEX]].tolist() if x <= limit]
            y_points = df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
            y_points = y_points[:len(x_points)]

            data_frame2 = data_frame[(data_frame[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                     & (data_frame[ALL_COLUMNS[METHOD_INDEX]]== method)
                                     & (data_frame[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                     & (data_frame[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)]
            minimum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).min()
            minimum = minimum[ALL_COLUMNS[DURATION_INDEX]].tolist()
            maximum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).max()
            maximum = maximum[ALL_COLUMNS[DURATION_INDEX]].tolist()
        
            mean = df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
            maxmean = [x-y for x,y in zip(maximum, mean)]
            meanmin = [y-x for x,y in zip(minimum, mean)]

            plt.errorbar(x_points
                         , y_points
                         , yerr=[meanmin[:len(x_points)], maxmean[:len(x_points)]]
                         , color=COLORS[index]
                         , linestyle=LINESTYLES[0]
                         , linewidth=1.5
                         , marker=MARKERS[index]
                         , label= str(nthreads)+" threads")

        """
        plt.plot(x_points
        , y_points
        , color=COLORS[index]
        , linestyle=LINESTYLES[0]
        , linewidth=1.5
        , marker=MARKERS[index]
        , label= method + ", "+str(nthreads)+" threads")
        """
        #Draw caches limit
        #for index,cache in enumerate(CACHES):
        #plt.axvline(x=(CACHES[cache]*BITS_PER_BYTE/pixel_bit)**0.5,label=cache,color=CACHES_COLORS[index])
    
        plt.xlim([0,float(limit*limit/MEGA)])
        plt.title(str(pixel_bit) + "-bit images, multi-threaded CTMF,\n"+test_case.lower() + " data", size=22, y=1.02)
        plt.ylabel(AVERAGE_DURATION, size=18)
        #plt.xlabel(RESOLUTION_COLUMN)
        plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
        plt.legend(loc="upper left", shadow=True, fancybox=True, prop={"size":15})
        plt.grid()
        plt.tick_params(labelsize=16)
        #plt.rc('font', **font)
        plt.savefig(str(pixel_bit)+"-bit_CTMF_parallized_"+test_case+ "_data_lower_scale.pdf",bbox_inches='tight',pad_inches = 0)
        plt.gcf().clear()
# 8 bits ctmf, all fully parallized at lower scale, ends

# 8 bit and 16 bits, single threaded algorithms without ctmf
pixel_bit = PIXEL_DEPTHS[-1]
for test_case in TEST_CASES:
    #for ls, pixel_bit in enumerate(PIXEL_DEPTHS):
    for index, method in enumerate(METHODS[1:]):
        df_single_thread = mean_df[(mean_df[ALL_COLUMNS[METHOD_INDEX]] == method)
                                   & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                   & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]==1)
                                   & (mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)]

        data_frame2 = data_frame[(data_frame[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                 & (data_frame[ALL_COLUMNS[METHOD_INDEX]]== method)
                                 & (data_frame[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                 & (data_frame[ALL_COLUMNS[NTHREADS_INDEX]]== 1)]
        
        minimum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).min()
        minimum = minimum[ALL_COLUMNS[DURATION_INDEX]].tolist()
        maximum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).max()
        maximum = maximum[ALL_COLUMNS[DURATION_INDEX]].tolist()
        
        mean = df_single_thread[ALL_COLUMNS[DURATION_INDEX]].tolist()
        maxmean = [x-y for x,y in zip(maximum, mean)]
        meanmin = [y-x for x,y in zip(minimum, mean)]

        plt.errorbar([x*x/MEGA for x in df_single_thread[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                     , df_single_thread[ALL_COLUMNS[DURATION_INDEX]].tolist()
                     , yerr=[meanmin, maxmean]
                     , color=COLORS[index]
                     , linestyle=LINESTYLES[0]
                     , linewidth=1.5
                     , marker=MARKERS[index]
                     , label= method)
        """
        
        plt.plot([x*x/MEGA for x in df_single_thread[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                 , df_single_thread[ALL_COLUMNS[DURATION_INDEX]].tolist()
                 , color=COLORS[index]
                 , linestyle=LINESTYLES[0]
                 , linewidth=1.5
                 , marker=MARKERS[index]
                 , label= str(pixel_bit) + "-bit " + method)
        """
    #for index,cache in enumerate(CACHES):
    #    plt.axvline(x=(CACHES[cache]*BITS_PER_BYTE/pixel_bit)**0.5,label=cache,color=CACHES_COLORS[index])

    plt.xlim(X_LIM)
    plt.title("16-bit images, no CTMF, single-threaded,\n"+test_case + " data", size=22, y=1.02)
    plt.ylabel(AVERAGE_DURATION, size=18)
    plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
    plt.legend(loc="upper left", shadow=True, fancybox=True, prop={"size":18})
    plt.grid()
    plt.tick_params(labelsize=16)
    #plt.rc('font', **font)
    plt.savefig("16-bit_all_single_threaded_"+test_case+"_data_no_ctmf.pdf",bbox_inches='tight',pad_inches = 0)
    plt.gcf().clear()
    #plt.xlabel(ALL_COLUMNS[RESOLUTION_INDEX])
    
# 16 bits, single threaded algorithms without ctmf, ends

# 16 bits, all (except ctmf) fully parallized, ends
pixel_bit = PIXEL_DEPTHS[-1]
for test_case in TEST_CASES:
    for index, method in enumerate(METHODS[1:]):
        nthreads = NTHREADS[-1]
        df_parallized_method = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                       &(mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                       & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                       & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)
        ]
        plt.plot([x*x/10**6 for x in df_parallized_method[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                 , df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                 , color=COLORS[index]
                 , linestyle=LINESTYLES[0]
                 , linewidth=1.5
                 , marker=MARKERS[index]
                 , label= method)

    #Draw caches limit
    #for index,cache in enumerate(CACHES):
    #    plt.axvline(x=(CACHES[cache]*BITS_PER_BYTE/pixel_bit)**0.5,label=cache,color=CACHES_COLORS[index])

    plt.xlim(X_LIM)
    plt.title(str(pixel_bit) + "-bit images, no CTMF, 24-threaded,\n"+test_case + " data", size=22, y=1.02)
    plt.ylabel(AVERAGE_DURATION, size=18)
    #plt.xlabel(RESOLUTION_COLUMN)
    plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
    plt.legend(loc="upper left", shadow=True, fancybox=True, prop={"size":18})
    plt.grid()
    plt.tick_params(labelsize=16)
    #plt.rc('font', **font)
    plt.savefig(str(pixel_bit)+"-bit_all_parallized_"+test_case+ "_data_no_ctmf.pdf",bbox_inches='tight',pad_inches = 0)
    plt.gcf().clear()
# 16 bits, all (except ctmf) fully parallized, ends



# x bits, threads utility methods, starts
for pixel_bit in PIXEL_DEPTHS:
    for test_case in TEST_CASES:
        threads_utility = []
        for method in METHODS:
            df_single_method = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                           &(mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                           & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                           & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== NTHREADS[0])
            ]
            
            single_thread_durations = df_single_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
            for index, nthreads in enumerate(NTHREADS[1:]):
                df_parallized_method = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                               &(mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                               & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                               & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)
                                               ]
                multi_thread_durations = df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                threads_utility = [si/mi for si,mi in zip(single_thread_durations, multi_thread_durations)]
                plt.plot([x*x/MEGA for x in df_parallized_method[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                         , threads_utility
                         , color=COLORS[index]
                         , linestyle=LINESTYLES[0]
                         , linewidth=1.5
                         , marker=MARKERS[index]
                         , label=str(nthreads)+" threads")
                
            plt.xlim(X_LIM)
            plt.ylim([0,max(threads_utility)+2])
            plt.yticks(range(0,int(max(threads_utility))+2,2))
            plt.title(str(pixel_bit) + "-bit images, speedup of " + METHODS_MAP[method] + ",\n" + test_case + " data", size=22, y=1.02)
            plt.ylabel("Speedup", size=18)
            #plt.xlabel(RESOLUTION_COLUMN)
            plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
            plt.legend(shadow=True, fancybox=True, prop={"size":18}, loc=9, bbox_to_anchor=(0.5, -0.1), ncol=2)
            plt.grid()
            plt.tick_params(labelsize=16)
            #plt.rc('font', **font2)
            plt.savefig("utility_" + str(pixel_bit)+"-bit_" + method.replace(" ","_")+ "_parallized_" + test_case+ ".pdf",bbox_inches='tight',pad_inches = 0)
            plt.gcf().clear()
# x bits, threads utility methods, ends

#exit()
# x-axis = number of threads, y-axis = duration, curves = methods, constants = Image Height/Width, starts
kateetti = RESOLUTIONS[-1]
for pixel_bit in PIXEL_DEPTHS:
    for test_case in TEST_CASES:
        df_nthreads = []
        #ctmf_16_bit = True
        for index, method in enumerate(METHODS):
            # avoid extremely slow ctmf in 16-bit
            ctmf_16_bit = (pixel_bit == PIXEL_DEPTHS[-1]) and (method == METHODS[0])
            if not ctmf_16_bit:
                df_nthreads = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                      & (mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                      & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                      & (mean_df[ALL_COLUMNS[RESOLUTION_INDEX]]== kateetti)]
                
                plt.plot(df_nthreads[ALL_COLUMNS[NTHREADS_INDEX]].tolist()
                         , df_nthreads[ALL_COLUMNS[DURATION_INDEX]].tolist()
                         , color=COLORS[index]
                         , linestyle=LINESTYLES[0]
                         , linewidth=1.5
                         , marker=MARKERS[index]
                         , label= method)
        # avoid extremely slow ctmf in 16-bit
        if not ctmf_16_bit:
            plt.xlim([0, NTHREADS[-1]+1])
            #plt.ylim([0,6000])
            plt.xticks(NTHREADS)
            #plt.ylim([0, df_nthreads[ALL_COLUMNS[DURATION_INDEX]].tolist())
            plt.yscale('log')
            plt.title(str(pixel_bit) + "-bit images, "+ str(int(kateetti*kateetti/MEGA))  +" megapixels,\n" + test_case + " data", size=22, y=1.02)
            plt.ylabel(AVERAGE_DURATION, size=18)
            plt.xlabel("Number of threads", size=18)
            plt.legend(loc="upper right", shadow=True, fancybox=True, prop={"size":18})
            plt.grid()
            plt.tick_params(labelsize=16)
            #plt.rc('font', **font)
            plt.savefig(str(pixel_bit)+"-bit_nthreads_" + test_case+ ".pdf",bbox_inches='tight',pad_inches = 0)
            plt.gcf().clear()
# x-axis = number of threads, y-axis = duration, curves = methods, constants = Image Height/Width, starts



# 16 bits / 8 bits, rational parallized methods, starts
for test_case in TEST_CASES:
    for method in METHODS:
        for index, nthreads in enumerate(NTHREADS):
            df_parallized_method8 = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == 8)
                                           & (mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                           & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                           & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)
            ]
            df_parallized_method16 = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == 16)
                                           & (mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                           & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                           & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)
            ]
            df_parallized_ratio = [y/x for x,y in zip(df_parallized_method8[ALL_COLUMNS[DURATION_INDEX]].tolist(), df_parallized_method16[ALL_COLUMNS[DURATION_INDEX]].tolist())]
            plt.plot([x*x/MEGA for x in df_parallized_method8[ALL_COLUMNS[RESOLUTION_INDEX]].tolist()]
                         , df_parallized_ratio
                         , color=COLORS[index]
                         , linestyle=LINESTYLES[0]
                         , linewidth=1.5
                         , marker=MARKERS[index]
                         , label= str(nthreads)+" threads")
            #Draw caches limit
            #for index,cache in enumerate(CACHES):
            #    plt.axvline(x=(CACHES[cache]*BITS_PER_BYTE/pixel_bit)**0.5,label=cache,color=CACHES_COLORS[index])

        plt.xlim(X_LIM)
        if method !=  METHODS[0]:
            plt.ylim([0.4,1.6])
        plt.title("16-bit vs. 8-bit images, " + METHODS_MAP[method] + ",\n" + test_case + " data", size=22, y=1.02)
        plt.ylabel("16-bit duration / 8-bit duration", size=18)
        #plt.xlabel(RESOLUTION_COLUMN)
        plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
        plt.legend(shadow=True, fancybox=True, prop={"size":18},loc=9, bbox_to_anchor=(0.5, -0.1), ncol=2)
        plt.grid()
        plt.tick_params(labelsize=16)
        #plt.rc('font', **font2)
        plt.savefig("ratio_" +  method.replace(" ","_") + "_"+test_case+ ".pdf",bbox_inches='tight',pad_inches = 0)
        plt.gcf().clear()
# 16 bits / 8 bits, rational parallized methods ends
