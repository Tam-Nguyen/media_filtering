//#define NDEBUG
#include <cassert>
#include <climits>
#include <cstring>
#include <omp.h>
#include <cstdlib>
#include <cstdint>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <chrono>
#include <thread>
#include <set>
#include "ctmf8.h"
#include "ctmf16.h"
#include <string>

//#define PRINT_RESULT_DEBUG
// verify results and heap properties
#define VERIFY_RESULTS
// verify heap properties
//#define VERIFY_HEAP
// verify swap
//#define VERIFY_SWAP


// RT = Research type = uint8_t | uint16_t | uint32_t
//#define RESEARCH_TYPE uint8_t
// Range Type = int32_t | int64_t | intmax_t.
// Range, or Range Type are for steppers and sizes to do computations.
#define RANGE_TYPE int32_t
/*
#define RESOLUTION_STEP 200
#define RESOLUTION_LIMIT 400
#define ITERATION_LIMIT 1
#define CORE_LIMIT 4
// half size = half height = half width = Radius
#define RADIUS_STEP 2
#define RADIUS_LIMIT 11*/
#define L2_CACHE 256*1024
//#define L3_CACHE 15360*1024
#define L3_CACHE  8192*1024 //Paniikin kone

template<class RangeType> inline RangeType maximum_integer(const RangeType x, const RangeType y)
{
  return x^((x^y) & -(x < y));
}

template<class RangeType> inline RangeType minimum_integer(const RangeType x, const RangeType y)
{
  return y^((x^y) & -(x < y));
}

template<class ResearchType, class RangeType> inline void print_array(const ResearchType * const array, const RangeType R, const RangeType C)
{
  for(RangeType y = 0; y < R; y++)
    {
      for(RangeType x = 0; x < C; x++)
        {
          printf("%4u ",array[y*R+x]);
          //std::cout << array[y*R + x] << " ";
        }
      std::cout << std::endl;
    }
}


#ifdef VERIFY_RESULTS
/*
  Verify that all inner medians of answer are same as model_answer.
  Inner medians are in  [0+radius, rows - radius]x[0+radius,columns-radius]
 */
template<class ResearchType, class RangeType> void verify_medians(const ResearchType * const model_answer, const ResearchType * const  answer, const RangeType rows, const RangeType columns, const RangeType radius, const RangeType original_threads)
{
  
  #pragma omp parallel for collapse(2) num_threads(omp_get_max_threads())
  for(RangeType row = radius; row < rows - radius; row++)
    {
      for(RangeType column = radius; column < columns - radius; column++)
        {
          if(model_answer[row*columns+column] != answer[row*columns+column])
            {
              printf("ERROR!! at y: %4u , x: %4u | values (model answer/answer): %4u != %4u | rows: %4u, columns: %4u, radius: %4u",row,column, model_answer[row*columns+column], answer[row*columns+column],rows,columns, radius);
              /*std::cout << "ERROR!! y: "<< row <<" x:"<< column <<", "
                      << model_answer[row*columns+column]
                      << "!=" << answer[row*columns+column]
                      << ", rows:" << rows
                      << ", columns: " << columns
                      << ", radius: " << radius
                      <<std::endl;*/
            abort();
            }
        }
    }

  // Reset thread number
  omp_set_num_threads(original_threads);

}
#endif


/*
  Heap class
*/
template<class ResearchType, class RangeType>
class Heap
{
public:
  /*
  ResearchType pop();
  virtual void heapify() ;
  */
  //ResearchType elements[(2*RADIUS+1)*(2*RADIUS+1)/2 + 1];
  ResearchType *elements;
  RangeType size;
  RangeType capacity;
  void insert(ResearchType);
  RangeType remove(ResearchType);
  Heap(RangeType radius)
  {
    size = 0;
    capacity = (2*radius+1)*(2*radius+1)/2 + 2;
    elements = new ResearchType[capacity];
   }
  ~Heap()
  {
    delete[] elements;
  }

  ResearchType peek(){ return elements[0];}
  
  RangeType parent(RangeType child){ return (child-1)/2; }
  RangeType left_child(RangeType parent){ return 2*parent+1; }
  RangeType right_child(RangeType parent){ return 2*parent+2; }
  RangeType getSize(){return size;}
  RangeType getArray(){return elements;}
  void clear(){size = 0;}
  
  void swap_parent(RangeType old_value_index, RangeType new_value_index)
  {
    ResearchType old_value = elements[old_value_index];
    elements[old_value_index] = elements[new_value_index];
    elements[new_value_index] = old_value;
  }
};
/*
  Heap class, ends
  Compare{}(a,b) = a < b || a > b. Used by max or min heap.
*/
template<class ResearchType, class RangeType, class Compare>
class MaxMinHeap : public Heap<ResearchType, RangeType>
{
 public:
  MaxMinHeap(RangeType radius):Heap<ResearchType, RangeType>(radius){}
  
  void insert(ResearchType e)
  {
    this->elements[this->size] = e;
    insert_heapify(this->size);
    this->size++;
    #ifdef VERIFY_HEAP
    assert(this->capacity >= this->size);
    #endif
  }
  RangeType remove(ResearchType e)
  {
    RangeType current_index = 0;
    ResearchType current_element = this->elements[current_index];
    while(current_element != e && current_index < this->size)
      {
        current_index++;
        current_element = this->elements[current_index];
      }
    #ifdef VERIFY_HEAP
    assert(current_element == e || fprintf(stderr, "%d != %d\n", current_element, e));
    #endif
    this->size--;
    remove_heapify(current_index);
    #ifdef VERIFY_HEAP
    assert(this->capacity >= this->size);
    #endif
    return current_element;      
  }

  
  ResearchType pop()
  {
    ResearchType e = this->elements[0];
    //remove-function in base class, so it needs virtual method (=0) as reference to remove function.
    remove(e);
    return e;
  }

private:

  /*
    Replace an parent_index with the last leaf and sort the heap.
    Below comments are for Min Heap.
   */
  void remove_heapify(RangeType parent_index)
  {

    // Replace the removing element by the furthest leaf.
    ResearchType parent = this->elements[this->size];
    this->swap_parent(parent_index, this->size);

    Compare comp = Compare{};
    /*
      Heapify downward, starts
     */
    RangeType lchild_index = this->left_child(parent_index);
    RangeType rchild_index = this->right_child(parent_index);
    while (lchild_index < this->size || rchild_index < this->size)
      {
        // two children
        if(lchild_index < this->size && rchild_index < this->size)
          {
            //Compare comp <=> Compare comp = {}
            // right child is smaller to sibling and right child is smaller than parent.
            if( comp(this->elements[rchild_index], this->elements[lchild_index]))
              {
                if (comp(this->elements[rchild_index],parent))
                  {
                    this->swap_parent(parent_index, rchild_index);
                    parent_index = rchild_index;
                  }
                // Children are greather than parent
                else {break;}
              }
            // left child is smaller or equal to sibling and left child is smaller than parent.
            else
              {
                if (comp(this->elements[lchild_index], parent))
                  {
                    this->swap_parent(parent_index, lchild_index);
                    parent_index = lchild_index;
                  }
                // Children are greather than parent
                else {break;}
              }
          }
        // one child
        else
          {
            // If left child exists and is greather than parent.
            if(lchild_index < this->size && comp(this->elements[lchild_index],parent))
              {
                this->swap_parent(parent_index, lchild_index);
                parent_index = lchild_index;
              }
            // Heap a; <=> Heap a{} ~ Heap a() <~> a = new Heap()
            // If right child exists and is greather than parent.
            else if(rchild_index < this->size && comp(this->elements[rchild_index],parent))
              {
                this->swap_parent(parent_index, lchild_index);
              }
            // None of children are greather than parent.
            else{break;}
          }
        lchild_index = this->left_child(parent_index);
        rchild_index = this->right_child(parent_index);
      }
    /*
      Heapify downward, ends
    */

    /*
      Heapify upward, starts
    */
    ResearchType child = this->elements[parent_index];
    RangeType child_index = parent_index;
    parent_index = this->parent(parent_index);
    while(parent_index > 0 && Compare{}(child, this->elements[parent_index]))
      {
        this->swap_parent(parent_index,child_index);
        child_index = parent_index;
        parent_index = this->parent(child_index);
      }
    /*
      Heapify upward, ends
    */
  }
  void insert_heapify(RangeType child_index)
  {
    Compare comp = Compare{};
    RangeType parent_index = this->parent(child_index);
    while(parent_index >= 0 && comp(this->elements[child_index], this->elements[parent_index]))
      {
        this->swap_parent(parent_index,child_index);
        child_index = parent_index;
        parent_index = this->parent(child_index);
      }
  }  
};

/*
Median heap, starts
*/
template<class ResearchType, class RangeType>
class MedianHeap
{
  public:
  MaxMinHeap<ResearchType, RangeType, std::less<ResearchType> > minheap;
  MaxMinHeap<ResearchType, RangeType, std::greater<ResearchType> > maxheap;
  
  MedianHeap(RangeType radius):minheap(radius),maxheap(radius)
  {
    //minheap = new MaxMinHeap<ResearchType, RangeType, std::less<ResearchType>>(radius);
    //maxheap = new MaxMinHeap<ResearchType, RangeType, std::greater<ResearchType>>(radius);
    //minheap(radius);
    //maxheap(radius);
  }
  ~MedianHeap(){}
  
  RangeType get_size(){return maxheap.size + minheap.size;};

  // This will return the median as an average of two median.
  ResearchType get_median()
  {
    #ifdef VERIFY_HEAP
    assert(get_size() > 0);
    #endif
    if (maxheap.size > minheap.size)
      {
        return maxheap.peek();
      }
    else if (minheap.size > maxheap.size)
      {
        return minheap.peek();
      }
    else
      {
        return (maxheap.peek() + minheap.peek()) >> 1;
      }
  }
  void clear(){minheap.clear();maxheap.clear();}
  /*
    Verify that all Max Heap children are smaller or equal to their parent.
  */
  void verify_max_heap_parents(const ResearchType * const heap, const RangeType heap_size)
  {
    for(RangeType child_index = 1; child_index < heap_size; child_index++)
      {
        RangeType parent_index = (child_index - 1) / 2;
        if(heap[parent_index] < heap[child_index])
          {
            std::cout << "A child is greater than parent in max heap. Parent index/value: " << parent_index << "/" << heap[parent_index]
                      << ". Child index/value: " << child_index << "/" << heap[child_index] << std::endl;
            abort();
          }
      }
  }
  
  /*
    Verify that all Min Heap children are greater or equal to their parent.
  */
  void verify_min_heap_parents(const ResearchType * const heap, const RangeType heap_size)
  {
    for(RangeType child_index = 1; child_index < heap_size; child_index++)
      {
        RangeType parent_index = (child_index - 1) / 2;
        if(heap[parent_index] > heap[child_index])
          {
            std::cout << "A child is smaller than parent in min heap. Parent index/value: " << parent_index << "/" << heap[parent_index]
                      << ". Child index/value: " << child_index << "/" << heap[child_index] << std::endl;
            abort();
          }
      }
  }
  
  void insert(ResearchType e)
  {
    // Median Heap is empty
    if(get_size() == 0)
      {

        maxheap.insert(e);
      }
    else
      {
        if(e <= get_median())
          {
            maxheap.insert(e);
            if(maxheap.size - minheap.size > 1)
              {
                minheap.insert(maxheap.pop());
              }
          }
        else
          {

            minheap.insert(e);
            if(minheap.size - maxheap.size > 1)
              {
                maxheap.insert(minheap.pop());
              }
          }
      }

    #ifdef VERIFY_HEAP
    verify_min_heap_parents(minheap.elements, minheap.size);
    verify_max_heap_parents(maxheap.elements, maxheap.size);
    #endif
  }

  void remove(ResearchType e)
  {
    #ifdef VERIFY_HEAP
    assert(get_size() > 0);
    #endif

    if(e <= maxheap.peek())
      {
        maxheap.remove(e);
        if(minheap.size - maxheap.size > 1)
          {
            maxheap.insert(minheap.pop());
          }
        return;
      }
    
    minheap.remove(e);
    if(maxheap.size - minheap.size > 1)
      {
        minheap.insert(maxheap.pop());
      }

    #ifdef VERIFY_HEAP
    verify_min_heap_parents(minheap.elements, minheap.size);
    verify_max_heap_parents(maxheap.elements, maxheap.size);
    #endif
  }
};

template<class ResearchType, class RangeType> void start(const RangeType hs, const RangeType H, const RangeType W, const ResearchType * const  image,  ResearchType * const result)
{
  /* 
     http://stackoverflow.com/questions/15304760/how-are-firstprivate-and-lastprivate-different-than-private-clauses-in-openmp
     http://stackoverflow.com/questions/33309890/openmp-lastprivate-and-firstprivate-to-the-same-variable
  */
  #pragma omp parallel
  {
    MedianHeap< ResearchType, RangeType> median_heap(hs);
    #pragma omp for
    for (RangeType y = 0; y < H; y++)
      {
        RangeType x=0;
        const RangeType y_min = ::maximum_integer(static_cast<RangeType>(0), y-hs);
        const RangeType y_max = ::minimum_integer(H-1,y+hs);

        // Setting initial elements for X-array. In this loop, assumes x=0.
        for(RangeType y_step = y_min; y_step <= y_max;y_step++)
          {
            for(RangeType x_step = 0, filter_row = y_step*W; x_step <= hs;x_step++)
              {
                median_heap.insert(image[x_step + filter_row]);
              }
          }
        /*
        std::cout<< "0. Min Heap" << std::endl;
        print_array(median_heap.minheap.elements, static_cast<RangeType>(1), median_heap.minheap.size);
        std::cout<< "0. Max Heap" << std::endl;
        print_array(median_heap.maxheap.elements, static_cast<RangeType>(1), median_heap.maxheap.size);
        */
        result[x+W*y] = median_heap.get_median();
        
        
        // Elements of X-array increases.
        for(x=1; x <= hs; x++)
          {
            
            for(RangeType y_step = y_min; y_step <= y_max; y_step++)
              {
                median_heap.insert(image[(x+hs)+y_step*W]);
              }
            #ifdef DEBUG
            std::cout<< "1. Min Heap" << std::endl;
            print_array(median_heap.minheap.elements, static_cast<RangeType>(1), median_heap.minheap.size);
            std::cout<< "1. Max Heap" << std::endl;
            print_array(median_heap.maxheap.elements, static_cast<RangeType>(1), median_heap.maxheap.size);
            #endif
            result[x+W*y] = median_heap.get_median();
            
          }
        
        // X-array is full. Replace old elements with new one.
        for(;x < W - hs; x++)
          {
            for(RangeType y_step = y_min; y_step <= y_max; y_step++)
              {
                median_heap.remove(image[(x-hs-1)+y_step*W]);
                median_heap.insert(image[(x+hs)+y_step*W]);
              }
            /*
            std::cout<< "2. Min Heap" << std::endl;
            print_array(median_heap.minheap.elements, static_cast<RangeType>(1), median_heap.minheap.size);
            std::cout<< "2. Max Heap" << std::endl;
            print_array(median_heap.maxheap.elements, static_cast<RangeType>(1), median_heap.maxheap.size);
            */
            result[x+W*y] = median_heap.get_median();
          }
        
        // Elements of X-array decreases. However, it will increase from right to left.
        for(; x < W; x++)
          {
            for(RangeType y_step = y_min; y_step <= y_max; y_step++)
              {
                median_heap.remove(image[(x-hs-1)+y_step*W]);
                //X[added] = image[x_step + filter_row];
              }
            /*
            std::cout<< "3. Min Heap" << std::endl;
            print_array(median_heap.minheap.elements, static_cast<RangeType>(1), median_heap.minheap.size);
            std::cout<< "3. Max Heap" << std::endl;
            print_array(median_heap.maxheap.elements, static_cast<RangeType>(1), median_heap.maxheap.size);
            result[x+W*y] = median_heap.get_median();
            */
            result[x+W*y] = median_heap.get_median();
            //set_median(x, y, W, added, X, result);
          }
        // set min and max heap sizes to zeroes.
        median_heap.clear();
      }
  }
}
/*
Median heap, ends
*/

/*
  Search old_value linearly from filter array and replace with an new_value.

  Notice: Can not use median information to determine which side should to look from filter array e.g. filter array: [...,100,100,100,...] and on next step replacing all 100 with new elements will produce errors.
 */
template<class ResearchType, class RangeType>inline void swap(ResearchType *filter, RangeType size, ResearchType old_value, ResearchType new_value)
{
  RangeType index = 0;
  if(filter[size>>1] <= old_value)
    {
      while(filter[index] != old_value && index < size) index++;
    }
  else
    {
      index = size-1;
      while(filter[index] != old_value && index >= 0) index--;
    }
  #ifdef VERIFY_SWAP
  assert(index < size && filter[index]==old_value);
  #endif
  filter[index] = new_value;
}

template<class ResearchType, class RangeType> inline void set_median(const RangeType x, const RangeType y, const RangeType W, const RangeType added, ResearchType * X, ResearchType * result)
{
 // Pick the median from the array X.
  RangeType middle = added >> 1;

  //If size is even, pick the left median element
  //middle = middle - (1-added % 2);
  std::nth_element(X,X+middle,X + added);
  ResearchType median = X[middle];
  //Set median based on the size of array, even or odd.
  switch(added % 2)
    {
      // Odd number of elements. The second median element is the maximum of left half.
    case 0:
      result[x + W*y] = (median + *(std::max_element(X,X+middle)))>>1;
      break;
      // Odd number of elements. The only median element is median1.
    default :
      result[x + W*y] = median;
      break;
    }
  //result[x + W*y] = median;
  //return median;
}

inline void print_debug()
{
  std::cout<< "DONE  " << std::endl;
}


// Brute force method.
template<class ResearchType, class RangeType> void naive(const RangeType radius, const RangeType H, const  RangeType W, const ResearchType * const image, ResearchType * const result)
{
  const RangeType filter_length = 2*radius+1;
  #pragma omp parallel
  {
    //ResearchType* X = new ResearchType[filter_length*filter_length];
    ResearchType X [filter_length*filter_length];
    #pragma omp for
    for (RangeType y = 0; y < H; y++)
      {
        for(RangeType x = 0, x_min, x_max, y_min = maximum_integer(static_cast<RangeType>(0),y-radius), y_max = minimum_integer(H-1,y+radius); x < W; x++)
          {
            
            //Two corner points of the filter window are (y_min,x_min) and (y_max, x_max)
            x_min = maximum_integer(static_cast<RangeType>(0),x-radius);
            x_max = minimum_integer(W-1, x+radius);
            
            RangeType added = 0;
            for(RangeType y_step = y_min; y_step <= y_max; y_step++)
              {
                for(RangeType x_step = x_min, filter_row = W * y_step; x_step <= x_max; x_step++)
                  {
                    X[added] = image[x_step + filter_row];
                    added++;
                  }
              }     
            set_median(x, y, W, added, X, result);
          }
      }
    //delete [] X;
  }
}

/*
DEBUG:
display *col_addresses@filter_length*filter_length
display *X@filter_length*filter_length
display *result@H*W
display added
*/
// Huang's O(n) median filtering algorithm.
// ResearchType = uint8_t | uint16_t | uint32_t
template<class ResearchType, class RangeType> void naive_improved(const RangeType radius, const RangeType H, const RangeType W, const ResearchType * const image,  ResearchType * const result)
{
  #pragma omp parallel
  {
    const RangeType filter_length = 2*radius+1;
    //ResearchType* X = new ResearchType[filter_length*filter_length];
    ResearchType X [filter_length*filter_length];
    #pragma omp for
    for (RangeType y = 0; y < H; y++)
      {
        RangeType x=0;
        RangeType added = 0;
        const RangeType y_min = maximum_integer(static_cast<RangeType>(0), y-radius);
        const RangeType y_max = minimum_integer(H-1,y+radius);

        // Setting initial elements for X-array. In this loop, assumes x=0.
        for(RangeType y_step = y_min; y_step <= y_max;y_step++)
          {
            for(RangeType x_step = 0, filter_row = y_step*W; x_step <= radius;x_step++)
              {
                X[added] = image[x_step + filter_row];
                added++;
              }
          }
        set_median(x, y, W, added, X, result);

        // Elements of X-array increases.
        for(x=1; x <= radius; x++)
          {
            
            for(RangeType y_step = y_min; y_step <= y_max; y_step++)
              {
                X[added] = image[(x+radius)+y_step*W];
                added++;
              }
            set_median(x, y, W, added, X, result);
          }
        
        // X-array is full. Replace old elements with new one.
        for(ResearchType old_value, new_value;x < W - radius; x++)
          {
            for(RangeType y_step = y_min; y_step <= y_max; y_step++)
              {
                old_value = image[(x-radius-1)+y_step*W];
                new_value = image[(x+radius)+y_step*W];
                swap(X, added, old_value, new_value);
              }
            set_median(x, y, W, added, X, result);
          }
        
        // Elements of X-array decreases. However, it will increase from right to left.

        // Fill elements from the corner. Everything except column W-radius-1
        added = 0;
        for(RangeType y_step = y_min; y_step <= y_max; y_step++)
          {
            for(RangeType x_step = W-1, filter_row = y_step*W; x_step >= W-radius; x_step--)
              {
                X[added] = image[x_step + filter_row];
                added++;
              }
          }
        // Continuesly add missing left column
        for(RangeType x_lower_bound = x, x = W - 1; x >= x_lower_bound; x--)
          {
            for(RangeType y_step = y_min, filter_row = y_step*W; y_step <= y_max; y_step++)
              {
                X[added] = image[(x - radius)+ filter_row];
                added++;
              }
            set_median(x, y, W, added, X, result);
          }
      }
    //delete [] X;
  }
}

/*
Below examples are decimal numeral system on 6x6 data.
  constant:
              5, 5, 5, 5, 5, 5;
              5, 5, 5, 5, 5, 5;
              5, 5, 5, 5, 5, 5;
              5, 5, 5, 5, 5, 5;
              5, 5, 5, 5, 5, 5;
              5, 5, 5, 5, 5, 5;

 ascending (descending):
              0, 1, 2, 3, 4, 5;
              6, 7, 8, 9, 0, 1;
              2, 3, 4, 5, 6, 7;
              8, 9, 0, 1, 2, 3;
              4, 5, 6, 7, 8, 9;
              0, 1, 2, 3, 4, 5;

 periodic ascending (descending), case radius is 1 below:
              0, 1, 2, 3, 4, 5;
              0, 1, 2, 3, 4, 5;
              0, 1, 2, 3, 4, 5;
              6, 7, 8, 9, 0, 1;
              6, 7, 8, 9, 0, 1;
              6, 7, 8, 9, 0, 1;
*/
//enum TestCases {constant, ascending, descending, random_default, periodic_ascending, periodic_descending, test_case_end};
//static const std::string test_cases[] = {"constant", "ascending", "descending", "random", "periodic ascending", "periodic descending"};

enum TestCases {constant, ascending, descending, random_default, test_case_end};
static const std::string test_cases[] = {"constant", "ascending", "descending", "random"};

template<class ResearchType, class RangeType> inline void get_test_cases(ResearchType * const image, const RangeType resolution, const int testcase)
{
  const RangeType SIZE=resolution*resolution;
  switch (testcase)
    {
    case constant:
      #pragma omp parallel for num_threads(omp_get_max_threads())
      for(RangeType i=0; i < SIZE; i++) image[i]=static_cast<ResearchType>(1);
      break;
    case ascending:
      #pragma omp parallel for num_threads(omp_get_max_threads())
      for(RangeType i=0; i < SIZE; i++) image[i]=static_cast<ResearchType>(i);
      break;
    case descending:
      #pragma omp parallel for num_threads(omp_get_max_threads())
      for(RangeType i=0; i < SIZE; i++) image[i]=static_cast<ResearchType>(~i);
      break;
    case random_default:
      // Generate Random Integers
      // std::uniform_int_distribution<ResearchType> uniform_dist(0,!static_cast<ResearchType>(0));
      std::generate(image,image+SIZE, std::rand);
      break;
    default:
      std::cout << "ERROR in get test case" << std::endl;
      abort();
    }
  
}



template<class RangeType> inline std::string construct_a_sample(const std::string pixel_depth, const std::string method, const int test_case, const RangeType resolution, const RangeType nthreads, const RangeType iteration, const std::chrono::duration<double, std::milli> elapsed_time, RangeType radius)
{
  // "Pixel depth|Method|Test Case|Resolution|Radius|Cores|Iteration #|Duration (ms)\n";
  return pixel_depth + "|" + method + "|" + test_cases[test_case] + "|"+ std::to_string(resolution)+"|" + std::to_string(radius) + "|"+std::to_string(nthreads) + "|" + std::to_string(iteration) + "|"+ std::to_string(elapsed_time.count())+ "\n";
}


template<class ResearchType, class RangeType> void benchmark
(const std::string &stats_file,
 const RangeType iteration_limit,
 const RangeType core_init,const RangeType core_limit,
 const RangeType resolution_init, const RangeType resolution_step, const RangeType resolution_limit,
 const RangeType radius_init, const RangeType radius_step, const RangeType radius_limit)
{
  std::ofstream statistics_file;
  statistics_file.open (stats_file, std::ios_base::app);
  std::string data;
 
  const std::string pixel_depth = std::to_string(sizeof(ResearchType)*CHAR_BIT);

  // Times for duration, starts
  std::chrono::high_resolution_clock::time_point start_time;
  std::chrono::high_resolution_clock::time_point end_time;
  std::chrono::duration<double, std::milli> elapsed;
  // Times for duration, ends

  //const RangeType radius = 2;
  for(RangeType resolution = resolution_init; resolution <= resolution_limit; resolution+=resolution_step)
    {
      const RangeType SIZE = resolution*resolution;
      ResearchType * const image = new ResearchType [SIZE]();
      ResearchType * const result = new ResearchType [SIZE]();
      ResearchType * const model_answer = new ResearchType [SIZE]();
      for(RangeType radius = radius_init; radius <= radius_limit; radius+=radius_step)
        {
          for(RangeType iteration = 1; iteration <= iteration_limit; iteration++)
            {              
              for(int test_case = constant;test_case != test_case_end;test_case++)
                {
                  get_test_cases(image,resolution,test_case);

                  #ifdef PRINT_RESULT_DEBUG
                  // Print data,starts
                  std::cout << std::string(10,'#') << test_cases[test_case] << std::string(10,'#') << std::endl;
                  std::cout << "*** Data ***" << std::endl;
                  print_array(image, resolution, resolution);
                  print_debug();         
                  // Print data,starts
                  #endif
              
                  std::fill(result,result+SIZE,0);          
                  for(RangeType cores = core_init ; cores <= core_limit ; cores <<=1 )
                    {
                      
                      // Limit threads number
                      omp_set_num_threads(cores);
                      
                      // Benchmark CTMF method, starts
                      if(typeid(image) == typeid(uint8_t*))
                        {
                          start_time = std::chrono::high_resolution_clock::now();
                          ctmf(reinterpret_cast<uint8_t*>(image), reinterpret_cast<uint8_t*>(result), resolution, resolution, resolution, resolution, static_cast<int>(radius), 1, L2_CACHE);
                          end_time = std::chrono::high_resolution_clock::now();
                          elapsed = end_time-start_time;
                        }
                      else
                        {
                          // 16 bit ctmf
                          start_time = std::chrono::high_resolution_clock::now();                     
                          ctmf16(reinterpret_cast<uint16_t*>(image), reinterpret_cast<uint16_t*>(result), resolution, resolution, resolution, resolution, static_cast<int>(radius), 1, L3_CACHE);
                          end_time = std::chrono::high_resolution_clock::now();
                          elapsed = end_time-start_time;
                        }
                      
                      statistics_file << construct_a_sample(pixel_depth, "CTMF", test_case, resolution, cores, iteration, elapsed, radius);
                      // Benchmark CTMF method, ends
                      
                      #ifdef VERIFY_RESULTS
                      // Save as a model answer, starts
                      std::memcpy(model_answer,result,SIZE*sizeof(ResearchType));
                      // verify_medians(model_answer,result,resolution,resolution, radius, cores);
                      // Save as a model answer, ends
                      #endif
                  
                      #ifdef PRINT_RESULT_DEBUG
                      // Print CTMF result, starts                      
                      std::cout << "*** CTMF results, cores :" << cores << ", radius: " << radius << "Color: " << sizeof(ResearchType)*CHAR_BIT << "-bits ***"<< std::endl;
                      print_array(result,resolution,resolution);
                      print_debug();
                      // Print CTMF result, ends
                      #endif
                      
                      
                      // Benchmark Naive method, starts
                      std::fill(result,result+SIZE,0);
                      start_time = std::chrono::high_resolution_clock::now();
                      //std::this_thread::sleep_for(std::chrono::seconds(1));
                      naive(radius, resolution, resolution, image, result);
                      end_time = std::chrono::high_resolution_clock::now();
                      elapsed = end_time-start_time;          

                      statistics_file << construct_a_sample(pixel_depth, "Naive", test_case, resolution, cores, iteration, elapsed, radius);
                      // Benchmark Naive method, ends
                  
                      #ifdef PRINT_RESULT_DEBUG
                      // Print Naive result, starts
                      std::cout << "*** Naive results, cores :" << cores << "***"<< std::endl;
                      print_array(result, resolution, resolution);
                      print_debug();
                      // Print Naive result, ends
                      #endif

                      #ifdef VERIFY_RESULTS
                      // Verify, starts
                      verify_medians(model_answer,result,resolution,resolution, radius, cores);
                      // std::memcpy(model_answer,result,SIZE*sizeof(ResearchType));
                      // Verify, ends
                      #endif
                      
                      
                      // Benchmark Naive Improved method, starts
                      std::fill(result,result+SIZE,0);
                      start_time = std::chrono::high_resolution_clock::now();
                      naive_improved(radius, resolution, resolution, image, result);
                      end_time = std::chrono::high_resolution_clock::now();
                      elapsed = end_time-start_time;
                      statistics_file << construct_a_sample(pixel_depth, "Improved Naive", test_case, resolution, cores, iteration, elapsed,radius);
                      // Benchmark Naive Improved method, ends

                      #ifdef PRINT_RESULT_DEBUG
                      // Print Naive Improved result, starts
                      std::cout << "*** Naive Improved results, cores :" << cores << ", radius: " << radius << "Color: " << sizeof(ResearchType)*CHAR_BIT << "-bits ***"<< std::endl;
                      print_array(result, resolution, resolution);
                      print_debug();
                      // Print Naive Improved result, ends
                      #endif

                      #ifdef VERIFY_RESULTS
                      // Verify, starts
                      verify_medians(model_answer,result,resolution,resolution, radius, cores);
                      // Verify, ends
                      #endif
                                          
                      // Benchmark Median Heap, starts
                      std::fill(result,result+SIZE,0);
                      start_time = std::chrono::high_resolution_clock::now();
                      start(radius, resolution, resolution, image, result);
                      end_time = std::chrono::high_resolution_clock::now();
                      elapsed = end_time-start_time;          
                      statistics_file << construct_a_sample(pixel_depth, "Median Heap", test_case, resolution, cores, iteration, elapsed,radius);
                      
                      // Benchmark Median Heap method, ends

                      #ifdef PRINT_RESULT_DEBUG
                      // Print Median Heap result, starts
                      std::cout << "*** Median Heap results, cores :" << cores << ", radius: " << radius << "Color: " << sizeof(ResearchType)*CHAR_BIT << "-bits ***"<< std::endl;
                      print_array(result, resolution, resolution);
                      print_debug();
                      // Print Median Heap result, ends
                      #endif

                      // use all threads
                      if((cores << 1) > core_limit && cores != core_limit)
                        {
                          cores = core_limit >> 1;
                        }
                      
                      #ifdef VERIFY_RESULTS
                      // Verify, starts
                      verify_medians(model_answer,result,resolution,resolution, radius, cores);
                      // Verify, ends
                      #endif
                      
                      statistics_file << std::flush;
                    }
                }
            }
        }

      delete [] image;
      delete [] result;
      delete [] model_answer;
      
    }
  statistics_file.close();
}

//using namespace std;
int main(int argc, char* argv[])
{
  if(argc == 0 && argv) return EXIT_FAILURE;
  
  /*
  //Debug sample
  const RANGE_TYPE resolution = 1000;
  RESEARCH_TYPE *image = new RESEARCH_TYPE[resolution*resolution];
  RESEARCH_TYPE *answer = new RESEARCH_TYPE[resolution*resolution];
  RESEARCH_TYPE *model_answer = new RESEARCH_TYPE[resolution*resolution];


  get_test_cases(image,resolution,ascending);
  //initialize answers to ones
  get_test_cases(answer,resolution, constant);
  get_test_cases(model_answer,resolution, constant);

  std::cout << "*** Data ***" << std::endl;
  //print_array(image, resolution, resolution);
  //RANGE_TYPE radius = 2;
  //naive(radius, resolution, resolution, image, model_answer);
  //ctmf16(image, answer, resolution, resolution, resolution, resolution, 2, 1,L3_CACHE);
  //print_array(answer, resolution, resolution);
  //verify_medians(model_answer, answer, resolution, resolution, radius, 1);
  
  for(RANGE_TYPE radius = 10; radius < 100; radius+=10)
    {
      naive(radius, resolution, resolution, image, model_answer);
      std::cout << "*** Naive Result *** Radius: " << radius << std::endl;
      //print_array(model_answer, resolution, resolution);
      start(radius, resolution, resolution, image, answer);
      std::cout << "*** Median Heap Result ***" << std::endl;
      //print_array(answer, resolution, resolution);
      verify_medians(model_answer, answer, resolution, resolution, radius, 1);
      std::cout << " VERIFICATION PASSED " << std::endl;
    }
  
  */
  
  omp_set_dynamic(0);     // Explicitly disable dynamic teams



  std::string stats_file = "raw_data_resolution.txt";
  std::string header = "Pixel depth|Method|Test Case|Resolution|Radius|Cores|Iteration #|Duration (ms)\n";
  std::ofstream statistics_file;
  statistics_file.open (stats_file);
  statistics_file << header;
  statistics_file.close();
  // Test process time of resolution, starts

  
  const RANGE_TYPE ITERATION_LIMIT = 10;
  const RANGE_TYPE CORE_INIT = 1;
  const RANGE_TYPE CORE_LIMIT = omp_get_max_threads();
  //const RANGE_TYPE RESOLUTION_INIT = 200, RESOLUTION_STEP = 200, RESOLUTION_LIMIT = 4000;
  //const RANGE_TYPE RADIUS_INIT = 2, RADIUS_STEP = 2, RADIUS_LIMIT = 2;
  /*
  //Benchmark 8 bits pixels
  benchmark<uint8_t, RANGE_TYPE>
    (stats_file,
     ITERATION_LIMIT,
     CORE_INIT, CORE_LIMIT,
     RESOLUTION_INIT, RESOLUTION_STEP, RESOLUTION_LIMIT,
     RADIUS_INIT, RADIUS_STEP, RADIUS_LIMIT
     );
  
  //Benchmark 16 bits pixels
  benchmark<uint16_t, RANGE_TYPE>
    (stats_file,
     ITERATION_LIMIT,
     CORE_INIT, CORE_LIMIT,
     RESOLUTION_INIT, RESOLUTION_STEP, RESOLUTION_LIMIT,
     RADIUS_INIT, RADIUS_STEP, RADIUS_LIMIT
     );
  // Test process time of resolution, ends
  */
  std::string stats_file2 = "raw_data_radius.txt";
  statistics_file.open (stats_file2);
  statistics_file << header;
  statistics_file.close();
  
  // Test process filter window size, starts
  const RANGE_TYPE RESOLUTION_INIT2 = 500 , RESOLUTION_STEP2 = 500, RESOLUTION_LIMIT2 = 500;
  const RANGE_TYPE RADIUS_INIT2 = 5, RADIUS_STEP2 = 5, RADIUS_LIMIT2 = 50;
  
    //Benchmark 8 bits pixels
  benchmark<uint8_t, RANGE_TYPE>
    (stats_file2,
     ITERATION_LIMIT,
     CORE_INIT, CORE_LIMIT,
     RESOLUTION_INIT2, RESOLUTION_STEP2, RESOLUTION_LIMIT2,
     RADIUS_INIT2, RADIUS_STEP2, RADIUS_LIMIT2
     );

  //Benchmark 16 bits pixels
  benchmark<uint16_t, RANGE_TYPE>
    (stats_file2,
     ITERATION_LIMIT,
     CORE_INIT, CORE_LIMIT,
     RESOLUTION_INIT2, RESOLUTION_STEP2, RESOLUTION_LIMIT2,
     RADIUS_INIT2, RADIUS_STEP2, RADIUS_LIMIT2
     );
    // Test process filter window size, ends 
    
  return EXIT_SUCCESS;
}
