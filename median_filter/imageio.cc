#include <Magick++.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <cstdlib>
#include "mf.h"
#include <chrono>
#include <iomanip>
#include <thread>
#include <pthread.h>
#include <unistd.h>
#define CHANNEL_COUNT 3
#define FIELD_WIDTH 15

// https://people.debian.org/~naoliv/misc/imagemagick/link/www/Magick++/
typedef struct RGB_info
{
  char *filename;
  std::string* file_format;
  size_t height;
  size_t width;
  size_t channel_count;
  float *R;
  float *G;
  float *B;
  float *R_result;
  float *G_result;
  float *B_result;
} RGB_info;

using namespace Magick;
using namespace std;
void print_mf_execution_time(RGB_info &rgb_info, int window_height, int window_width, chrono::milliseconds duration)
{
  //cout << rgb_info.filename << "\t" << rgb_info.height << "\t" << rgb_info.width << "\t" << window_height << "\t"  << window_width << "\t" << duration.count() << endl;

  cout << left << setw(FIELD_WIDTH)
       << rgb_info.filename << "\t"
       << left << setw(FIELD_WIDTH)
       << rgb_info.height << "\t"
       << left << setw(FIELD_WIDTH)
       << rgb_info.width << "\t"
       << left << setw(FIELD_WIDTH)
       << window_height << "\t"
       << left << setw(FIELD_WIDTH)
       << window_width << "\t"
       << left << setw(FIELD_WIDTH)
       << duration.count() << endl;
}
bool get_RGB(RGB_info &rgb_info)
{

  // https://www.imagemagick.org/Magick++/Image++.html
  Image image(rgb_info.filename);

  // Image image("cow.png"); 
  // Ensure that there are no other references to this image.
  // image.modifyImage();
  // Set the image type to TrueColor DirectClass representation.
  image.type(TrueColorType);

  Geometry geo = image.size();
  size_t h = rgb_info.height = geo.height();
  size_t w = rgb_info.width = geo.width();
  rgb_info.file_format = new string(rgb_info.filename+strlen(rgb_info.filename)-4);
  

  rgb_info.R = (float *)calloc(h*w,sizeof(float));
  rgb_info.G = (float *)calloc(h*w,sizeof(float));
  rgb_info.B = (float *)calloc(h*w,sizeof(float));
  rgb_info.R_result = (float *)calloc(h*w,sizeof(float));
  rgb_info.G_result = (float *)calloc(h*w,sizeof(float));
  rgb_info.B_result = (float *)calloc(h*w,sizeof(float));

  // Write to rgb_info.R, rgb_info.G and rgb_info.B
  image.write(0,0,w,h,"R",FloatPixel,rgb_info.R);
  image.write(0,0,w,h,"G",FloatPixel,rgb_info.G);
  image.write(0,0,w,h,"B",FloatPixel,rgb_info.B);

  /*
  for(unsigned int i = 0, j = 150; i < h; i++)
    {
      rgb_info.R[i] /= 2;
      //printf("(%f, %f, %f)\t",rgb_info.R[i+w*j], rgb_info.G[i+w*j], rgb_info.B[i+w*j]);
    }
  */
  return true;

}

bool write_image(RGB_info &rgb_info)
{
  float *image_colours = (float *)calloc(rgb_info.width*rgb_info.height*CHANNEL_COUNT,sizeof(float));
  for(unsigned int row = 0; row < rgb_info.height ; row++)
    {
      for(unsigned int column = 0; column < rgb_info.width; column++)
	{
	  
	  image_colours[column*CHANNEL_COUNT+row*rgb_info.width*CHANNEL_COUNT] = rgb_info.R_result[rgb_info.width*row+column];
	  image_colours[column*CHANNEL_COUNT+row*rgb_info.width*CHANNEL_COUNT + 1] = rgb_info.G_result[rgb_info.width*row+column];
	  image_colours[column*CHANNEL_COUNT+row*rgb_info.width*CHANNEL_COUNT + 2] = rgb_info.B_result[rgb_info.width*row+column];
	  /*
	  image_colours[column*CHANNEL_COUNT+row*rgb_info.width*CHANNEL_COUNT] = rgb_info.R[rgb_info.width*row+column];
	  image_colours[column*CHANNEL_COUNT+row*rgb_info.width*CHANNEL_COUNT + 1] = rgb_info.G[rgb_info.width*row+column];
	  image_colours[column*CHANNEL_COUNT+row*rgb_info.width*CHANNEL_COUNT + 2] = rgb_info.B[rgb_info.width*row+column];
	  */
	}
    }
  Image image;
  image.read(rgb_info.width,rgb_info.height,"RGB",FloatPixel,image_colours);
  image.write("temp"+*(rgb_info.file_format));
  free(image_colours);
  
  return true;
}

void clean_memory(RGB_info &rgb_info)
{
  free(rgb_info.R);
  free(rgb_info.G);
  free(rgb_info.B);

  free(rgb_info.R_result);
  free(rgb_info.G_result);
  free(rgb_info.B_result);

  delete rgb_info.file_format;
}

// Useage: ./program_name image_file
// image used: https://niksula.hut.fi/~qnguyen/unnamed.png
int main(int argc, char *argv[])
{
  if (argc != 4)
    {
      cout << "Useage: ./program_name image_file window_height window_width" << endl;
      return EXIT_FAILURE;
    }
  //RGB_info* rgb_info = (RGB_info*)malloc(sizeof(RGB_info));
  RGB_info rgb_info = {0};
  rgb_info.filename = argv[1];
  get_RGB(rgb_info);

  int hx = atoi(argv[2]);
  int hy = atoi(argv[3]);

  cout << "Image Name\tImage Height\tImage Width\tWindow Height\tWindow Width\tMF Duration (ms)"<<endl;
  
  // https://www.quora.com/What-is-the-easiest-way-to-calculate-the-time-elapsed-in-C++
  // https://solarianprogrammer.com/2012/10/14/cpp-11-timing-code-performance/
  chrono::high_resolution_clock::time_point t1,t2;

  t1 = chrono::high_resolution_clock::now();
  mf(static_cast<int>(rgb_info.height),static_cast<int>(rgb_info.width),hy,hx,rgb_info.R,rgb_info.R_result);
  t2 = chrono::high_resolution_clock::now();
  print_mf_execution_time(rgb_info,hy,hx,chrono::duration_cast<chrono::milliseconds>(t2-t1));

  t1 = chrono::high_resolution_clock::now();
  mf(static_cast<int>(rgb_info.height),static_cast<int>(rgb_info.width),hy,hx,rgb_info.G,rgb_info.G_result);
  t2 = chrono::high_resolution_clock::now();
  print_mf_execution_time(rgb_info,hy,hx,chrono::duration_cast<chrono::milliseconds>(t2-t1));

  t1 = chrono::high_resolution_clock::now();
  mf(static_cast<int>(rgb_info.height),static_cast<int>(rgb_info.width),hy,hx,rgb_info.B,rgb_info.B_result);
  t2 = chrono::high_resolution_clock::now();
  print_mf_execution_time(rgb_info,hy,hx,chrono::duration_cast<chrono::milliseconds>(t2-t1));

  //Write to file
  write_image(rgb_info);
  
  /*for(int i=0;i < 50 ; i++){
    cout << rgb_info.R_result[i] << "\t";
  }
  cout << "\n\n\n";
  */
  clean_memory(rgb_info);
  return EXIT_SUCCESS;
  /*
  using namespace::Magick;
  using namespace::std;
  if(argc<2) return 1;
  // load to constructor, main memory
  Image image(argv[1]);

  // Image image("cow.png"); 
  // Ensure that there are no other references to this image.
  image.modifyImage();
  // Set the image type to TrueColor DirectClass representation.
  image.type(TrueColorType);

  Geometry geo = image.size();
  size_t h = geo.height();
  size_t w = geo.width();
  float *rgb_colours_buf = (float*)calloc(h*w*3,sizeof(float));
  float *colour_buf = (float*)calloc(h*w,sizeof(float));  
  const string colours = "RGB";

  // Write to buf array
  image.write(0,0,w,h,colours,FloatPixel,rgb_colours_buf);
  unsigned int colours_size = colours.size();
  for(unsigned int colour_index = 0; colour_index < colours_size ; colour_index++)
    {
      //image.write(0,0,w,h,string(&colours[colour_index],1),FloatPixel,colour_buf);
      image.write(0,0,w,h,"R",FloatPixel,colour_buf);
      // Will replace with median filter.
      for(unsigned int column = 0;column<w;column++)
	{
	  for(unsigned int row = 0; row < h ; row++)
	    {
	      colour_buf[column+row*w] /= 2;
	      //buf[column*3+row*w*3] /= 2;
	      //buf[column*3+1+row*w*3] /= 2;
	      //buf[column*3+2+row*w*3] /= 2;
	    }
	}

      // Copy to all colours buffer.
      for(unsigned int column = 0; column < w ; column++)
	{
	  for(unsigned int row = 0; row < h ; row++)
	    {
	      rgb_colours_buf[column*colours_size+row*w*colours_size + colour_index] = colour_buf[column+row*w];
	    }
	}
    }
  // write from buf to Image
  image.read(w,h, colours,FloatPixel,rgb_colours_buf);
  */
  /*
  // Request pixel region with size 60x40, and top origin at 20x30 
  ssize_t columns = 200; 
  PixelPacket *pixel_cache = image.getPixels(0,0,200,200); 
  // Set pixel at column 5, and row 10 in the pixel cache to red. 
  ssize_t column = 5; 
  ssize_t row = 10;
  for(int column = 0;column<200;column++)
  {
  for(int row = 0; row < 200 ; row++)
  {
  PixelPacket *pixel = pixel_cache+row*columns+column; 
  *pixel = Color("red");
  cout << *pixel << endl;
  }
  }

  // Save changes to underlying image .
  image.syncPixels();
  */
  // Save updated image to file.
  //image.write("temp.jpg");
}
