#ifndef CTMF16_H
#define CTMF16_H

#ifdef __cplusplus
extern "C" {
#endif

void ctmf16(
        const uint16_t* src, uint16_t* dst,
        int width, int height,
        int src_step_row, int dst_step_row,
        int r, int channels, unsigned long memsize
        );

#ifdef __cplusplus
}
#endif

#endif
