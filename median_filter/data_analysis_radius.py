#!/usr/bin/python3

#with plt plot only
# http://stackoverflow.com/questions/21254472/multiple-plot-in-one-figure-in-python
# example
# plt.plot(method_df["Resolution"].tolist(),method_df['Duration (ms)'],label='Naive',marker='+')


import matplotlib.pyplot as plt
import csv
import pandas
import sys
SEP = "|"
METHOD_COLUMN = "Method"
PIXEL_COLUMN = "Pixel depth"
RESOLUTION_COLUMN = "Resolution"
CORES_COLUMN = "Cores"
ITERATION_COLUMN = 'Iteration #'
DURATION_COLUMN = 'duration (ms)'
AVERAGE_DURATION = "Average " + DURATION_COLUMN
BITS_PER_BYTE = 8
METHODS = ["CTMF", "Naive", "Naive Improved", "Median Heap"]
METHODS_MAP={"CTMF":"CTMF", "Naive":"naive", "Improved Naive":"improved naive", "Median Heap":"median heap"}
COLORS = ["#ff0000", "#00ff00", "#0000ff", "#FFA500", "#A9A9A9","#D2B48C", "#D2691E", "#800080", "#CD853F", "#808000", "#000080", "#FF00FF"]
LINESTYLES = ['-', '--','-.',':']
MARKERS = ['o','s','*','^','v','|','x','H','p','v','<','>','D','+']

font = {'family' : 'monospace', 'weight':'normal', 'size': 10}
font2 = {'family' : 'monospace', 'weight':'normal', 'size': 5}

CACHES = {"L1 D/I Cache":32*1024/10**6,"L2 Cache":256*1024/10**6, "L3 Cache":15360*1024/10**6}
CACHES_COLORS = ["#DCDCDC", "#696969", "#2F4F4F"]

X_AXIS_PIXEL_TITLE = "Filter radius (in pixels)"


data_frame = pandas.read_csv(sys.argv[1],sep=SEP)
#get all columns names
ALL_COLUMNS = data_frame.columns.values.tolist()
#get means. ALL_COLUMNS[:-2] means all except two last columns.
mean_df = data_frame.groupby(ALL_COLUMNS[:-2], as_index=False).mean()
#drop the iteration column inplace.
mean_df.drop(ALL_COLUMNS[-2], axis=1, inplace=True)

# "Pixel depth|Method|Test Case|Resolution|Radius|Cores|Iteration #|Duration (ms)\n";
PIXEL_INDEX, METHOD_INDEX, TEST_CASE_INDEX, RESOLUTION_INDEX, RADIUS_INDEX, NTHREADS_INDEX, ITERATION_INDEX, DURATION_INDEX = range(8)

PIXEL_DEPTHS = sorted(mean_df[ALL_COLUMNS[PIXEL_INDEX]].drop_duplicates().tolist())
METHODS = sorted(mean_df[ALL_COLUMNS[METHOD_INDEX]].drop_duplicates().tolist())
TEST_CASES = sorted(mean_df[ALL_COLUMNS[TEST_CASE_INDEX]].drop_duplicates().tolist())
NTHREADS = sorted(mean_df[ALL_COLUMNS[NTHREADS_INDEX]].drop_duplicates().tolist())
RADIUS = sorted(mean_df[ALL_COLUMNS[RADIUS_INDEX]].drop_duplicates().tolist())
RESOLUTIONS = sorted(mean_df[ALL_COLUMNS[RESOLUTION_INDEX]].drop_duplicates().tolist())

X_LIM = [0.0, max(RADIUS)+5]

# x bits, parallized methods, starts
def methods_parallelized():
    for pixel_bit in PIXEL_DEPTHS:
        for test_case in TEST_CASES:
            for method in METHODS:
                for index, nthreads in enumerate(NTHREADS):
                    df_parallized_method = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                                   & (mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                                   & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                                   & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)]

                    data_frame2 = data_frame[(data_frame[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                            & (data_frame[ALL_COLUMNS[METHOD_INDEX]]== method)
                                            & (data_frame[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                            & (data_frame[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)]
                    minimum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).min()
                    minimum = minimum[ALL_COLUMNS[DURATION_INDEX]].tolist()
                    maximum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).max()
                    maximum = maximum[ALL_COLUMNS[DURATION_INDEX]].tolist()
            
                    mean = df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                    maxmean = [x-y for x,y in zip(maximum, mean)]
                    meanmin = [y-x for x,y in zip(minimum, mean)]
                    plt.errorbar(df_parallized_method[ALL_COLUMNS[RADIUS_INDEX]].tolist()
                                 , df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                                 , yerr=[meanmin, maxmean]
                                 , color=COLORS[index]
                                 , linestyle=LINESTYLES[0]
                                 , linewidth=1.5
                                 , marker=MARKERS[index]
                                 , label= method + ", "+str(nthreads)+" thr.")


                if (method != METHODS[0]) and not (method==METHODS[1] and test_case==TEST_CASES[1] and pixel_bit == PIXEL_DEPTHS[1]):
                    # Draw 8-bit CTMF single threaded if differ
                    df_ctmf_single = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                             & (mean_df[ALL_COLUMNS[METHOD_INDEX]]== METHODS[0])
                                             & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                             & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== 1)]

                    data_frame2 = data_frame[(data_frame[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                             & (data_frame[ALL_COLUMNS[METHOD_INDEX]]== METHODS[0])
                                             & (data_frame[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                             & (data_frame[ALL_COLUMNS[NTHREADS_INDEX]]== 1)]
                    minimum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).min()
                    minimum = minimum[ALL_COLUMNS[DURATION_INDEX]].tolist()
                    maximum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).max()
                    maximum = maximum[ALL_COLUMNS[DURATION_INDEX]].tolist()
                
                    mean = df_ctmf_single[ALL_COLUMNS[DURATION_INDEX]].tolist()
                    maxmean = [x-y for x,y in zip(maximum, mean)]
                    meanmin = [y-x for x,y in zip(minimum, mean)]
                    plt.errorbar(df_ctmf_single[ALL_COLUMNS[RADIUS_INDEX]].tolist()
                                 , df_ctmf_single[ALL_COLUMNS[DURATION_INDEX]].tolist()
                                 , yerr=[meanmin, maxmean]
                                 , color=COLORS[index+1]
                                 , linestyle=LINESTYLES[1]
                                 , linewidth=1.5
                                 , marker=MARKERS[index+1]
                                 , label= METHODS[0] + ", 1 thr." )
                plt.xlim(X_LIM)
                plt.title(str(pixel_bit) + "-bit images, multi-threaded " + METHODS_MAP[method] + ", \n" + test_case + " data", size=20, y=1.02)
                plt.ylabel(AVERAGE_DURATION, size=16)
                plt.xlabel(X_AXIS_PIXEL_TITLE, size=16)
                if pixel_bit == PIXEL_DEPTHS[0] and method == METHODS[0]:
                    leg = plt.legend(shadow=True, fancybox=True, prop={"size":16}, loc=9, bbox_to_anchor=(0.5, -0.1), ncol=2)
                elif pixel_bit == PIXEL_DEPTHS[1] and method == METHODS[0]:
                    leg = plt.legend(shadow=True, fancybox=True, prop={"size":18}, loc="upper right")
                elif pixel_bit == PIXEL_DEPTHS[0] and method == METHODS[2] and test_case == "constant":
                    leg = plt.legend(shadow=True, fancybox=True, prop={"size":14}, loc="upper left")
                elif pixel_bit == PIXEL_DEPTHS[1] and method == METHODS[2] and test_case == "constant":
                    leg = plt.legend(shadow=True, fancybox=True, prop={"size":14}, loc="upper right")
                else:
                    leg = plt.legend(loc="upper left", shadow=True, fancybox=True, prop={"size":16})
                #leg.get_frame().set_alpha(0.5)
                plt.grid()
                plt.tick_params(labelsize=15)
                #plt.rc('font', **font)
                #if method == METHODS[0]:
                 #   leg.get_frame().set_alpha(0.5)
                    #plt.legend(loc="upper right")
                    
                plt.savefig(str(pixel_bit)+"-bit_radius_" + method.replace(" ","_") + "_parallized_"+test_case+ ".pdf", bbox_inches='tight',pad_inches = 0)
                plt.gcf().clear()
# x bits, parallized methods ends

# x bits, single threaded algorithms, starts
def single_threaded_methods():
    for pixel_bit in PIXEL_DEPTHS:
        for test_case in TEST_CASES:
            for index, method in enumerate(METHODS):
                df_single_thread = mean_df[(mean_df[ALL_COLUMNS[METHOD_INDEX]] == method)
                                           & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                           & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]==1)
                                           & (mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)]

                data_frame2 = data_frame[(data_frame[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                         & (data_frame[ALL_COLUMNS[METHOD_INDEX]]== method)
                                         & (data_frame[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                         & (data_frame[ALL_COLUMNS[NTHREADS_INDEX]]== 1)]
                minimum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).min()
                minimum = minimum[ALL_COLUMNS[DURATION_INDEX]].tolist()
                maximum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).max()
                maximum = maximum[ALL_COLUMNS[DURATION_INDEX]].tolist()
            
                mean = df_single_thread[ALL_COLUMNS[DURATION_INDEX]].tolist()
                maxmean = [x-y for x,y in zip(maximum, mean)]
                meanmin = [y-x for x,y in zip(minimum, mean)]
                plt.errorbar(df_single_thread[ALL_COLUMNS[RADIUS_INDEX]].tolist()
                             , df_single_thread[ALL_COLUMNS[DURATION_INDEX]].tolist()
                             , yerr=[meanmin, maxmean]
                             , color=COLORS[index]
                             , linestyle=LINESTYLES[0]
                             , linewidth=1.5
                             , marker=MARKERS[index]
                             , label= method)

            plt.xlim(X_LIM)
            plt.title(str(pixel_bit) + "-bit images, single-threaded,\n"+test_case + " data", size=20,y=1.02)
            plt.ylabel(AVERAGE_DURATION, size=18)
            plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
            plt.legend(loc="upper left", shadow=True, fancybox=True)
            plt.grid()
            #plt.rc('font', **font)
            plt.savefig(str(pixel_bit)+"-bit_radius_all_single_threaded_"+test_case+"_data.pdf",bbox_inches='tight',pad_inches = 0)
            plt.gcf().clear()
# x bits, single threaded algorithms, ends

def  all_fully_parallelized():
# x bits, all fully parallized, starts
    for pixel_bit in PIXEL_DEPTHS:
        for test_case in TEST_CASES:
            for index, method in enumerate(METHODS):
                nthreads = NTHREADS[-1]
                df_parallized_method = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                               &(mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                               & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                               & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)
                                           ]
                data_frame2 = data_frame[(data_frame[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                         & (data_frame[ALL_COLUMNS[METHOD_INDEX]]== method)
                                         & (data_frame[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                         & (data_frame[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)]
                minimum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).min()
                minimum = minimum[ALL_COLUMNS[DURATION_INDEX]].tolist()
                maximum = data_frame2.groupby(ALL_COLUMNS[:-2], as_index=False).max()
                maximum = maximum[ALL_COLUMNS[DURATION_INDEX]].tolist()
            
                mean = df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                maxmean = [x-y for x,y in zip(maximum, mean)]
                meanmin = [y-x for x,y in zip(minimum, mean)]
                plt.errorbar(df_parallized_method[ALL_COLUMNS[RADIUS_INDEX]].tolist()
                             , df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                             , yerr=[meanmin, maxmean]
                             , color=COLORS[index]
                             , linestyle=LINESTYLES[0]
                             , linewidth=1.5
                             , marker=MARKERS[index]
                             , label= method + ", "+str(nthreads)+" threads")
                
            plt.xlim(X_LIM)
            plt.title(str(pixel_bit) + "-bit images, multi-threaded,\n"+test_case + " data", size=22,y=1.02)
            plt.ylabel(AVERAGE_DURATION, size=18)
            #plt.xlabel(RESOLUTION_COLUMN)
            plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
            plt.legend(loc="upper left", shadow=True, fancybox=True, prop={"size":16})
            plt.grid()
            #plt.rc('font', **font)
            plt.savefig(str(pixel_bit)+"-bit_radius_all_parallized_"+test_case+ "_data.pdf", bbox_inches='tight',pad_inches = 0)
            plt.gcf().clear()
# x bits, all fully parallized, ends

def speedup():
    # x bits, threads utility methods, starts
    for pixel_bit in PIXEL_DEPTHS:
        for test_case in TEST_CASES:
            threads_utility = []
            for method in METHODS:
                df_single_method = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                           &(mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                           & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                           & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== NTHREADS[0])
                                       ]
            
                single_thread_durations = df_single_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                for index, nthreads in enumerate(NTHREADS[1:]):
                    df_parallized_method = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == pixel_bit)
                                                   &(mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                                   & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                                   & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)
                                               ]
                    multi_thread_durations = df_parallized_method[ALL_COLUMNS[DURATION_INDEX]].tolist()
                    threads_utility = [si/mi for si,mi in zip(single_thread_durations, multi_thread_durations)]
                    plt.plot(df_parallized_method[ALL_COLUMNS[RADIUS_INDEX]].tolist()
                             , threads_utility
                             , color=COLORS[index]
                             , linestyle=LINESTYLES[0]
                             , linewidth=1.5
                             , marker=MARKERS[index]
                             , label= str(nthreads)+" threads")
                
                plt.xlim(X_LIM)
                #if method == METHODS[0]:
                #    plt.ylim([0,max(threads_utility)+4])
                #else:
                #    plt.ylim([0,max(threads_utility)+10])
                plt.ylim([0,max(threads_utility)+2])
                plt.yticks(range(int(max(threads_utility))+2))
                plt.title(str(pixel_bit) + "-bit images, speedup of " + METHODS_MAP[method]+ ",\n" + test_case + " data", size=22, y=1.02)
                plt.ylabel("Speedup", size=18)
                #plt.xlabel(RESOLUTION_COLUMN)
                plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
                plt.legend(shadow=True, fancybox=True, prop={"size":18}, loc=9, bbox_to_anchor=(0.5, -0.1), ncol=2)
                plt.grid()
                #plt.rc('font', **font)
                plt.savefig("utility_" + str(pixel_bit)+"-bit_radius_" + method.replace(" ","_")+ "_parallized_" + test_case+ ".pdf", bbox_inches='tight',pad_inches = 0)
                plt.gcf().clear()
# x bits, threads utility methods, ends


def relative_duration_time():
    # 16 bits / 8 bits, rational parallized methods, starts
    for test_case in TEST_CASES:
        for method in METHODS:
            for index, nthreads in enumerate(NTHREADS):
                df_parallized_method8 = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == 8)
                                                & (mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                                & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                                & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)
                                            ]
                df_parallized_method16 = mean_df[(mean_df[ALL_COLUMNS[PIXEL_INDEX]] == 16)
                                                 & (mean_df[ALL_COLUMNS[METHOD_INDEX]]== method)
                                                 & (mean_df[ALL_COLUMNS[TEST_CASE_INDEX]] == test_case)
                                                 & (mean_df[ALL_COLUMNS[NTHREADS_INDEX]]== nthreads)
                                             ]
                df_parallized_ratio = [y/x for x,y in zip(df_parallized_method8[ALL_COLUMNS[DURATION_INDEX]].tolist(), df_parallized_method16[ALL_COLUMNS[DURATION_INDEX]].tolist())]
                plt.plot(df_parallized_method8[ALL_COLUMNS[RADIUS_INDEX]].tolist()
                         , df_parallized_ratio
                         , color=COLORS[index]
                         , linestyle=LINESTYLES[0]
                         , linewidth=1.5
                         , marker=MARKERS[index]
                         , label= str(nthreads)+" threads")

            plt.xlim(X_LIM)
            #if method !=  METHODS[0]:
            #   plt.ylim([0,2.0])
            plt.title("16-bit vs. 8-bit images, " + METHODS_MAP[method] + ",\n" + test_case + " data", size=22, y=1.02)
            #plt.title(method.title() + ", " + test_case.title() + ", 16-bit vs. 8-bit")
            plt.ylabel("16-bit duration / 8-bit duration", size=18)
            #plt.xlabel(RESOLUTION_COLUMN)
            plt.xlabel(X_AXIS_PIXEL_TITLE, size=18)
            plt.legend(shadow=True, fancybox=True, prop={"size":18},loc=9, bbox_to_anchor=(0.5, -0.1), ncol=2)
            #plt.legend(loc="upper left", shadow=True, fancybox=True)
            plt.grid()
            #plt.rc('font', **font)
            plt.savefig("ratio_radius_" +  method.replace(" ","_") + "_"+test_case+ ".pdf",bbox_inches='tight',pad_inches = 0)
            plt.gcf().clear()
# 16 bits / 8 bits, rational parallized methods ends

def main():
    methods_parallelized()
    single_threaded_methods()
    all_fully_parallelized()
    speedup()
    relative_duration_time()
main()
