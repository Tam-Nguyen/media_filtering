#include "mf.h"
#include <vector>
#include <algorithm>
#include <iostream>
#include <stdlib.h>

//perf stat -e L1-dcache-load-misses,L1-dcache-store-misses,L1-dcache-prefetch-misses,L1-icache-load-misses,LLC-loads,LLC-stores,LLC-prefetches ./mf-benchmark 1000 1000 10
using namespace std;
void mf(int ny, int nx, int hy, int hx, const float* in, float* out) {


  // Why double loop is faster than single loop?????
  // Why defining  int vector_size outside is slower than using v.size()???
  // Why moving x_min, x_max, y_min and y_max around make performance bad.
  #pragma omp parallel
  {
    float *v = (float*)calloc(nx*ny,sizeof(float));
    #pragma omp for
    for (int y = 0; y < ny; ++y)
      {
	for(int x=0, x_min, x_max, y_min = max(0,y-hy), y_max = min(ny-1,y+hy), middle; x < nx; ++x)
	  {
	    // window four corner points.
	    // y corners moved to for loop.
	    x_min = max(0,x-hx);
	    x_max = min(nx-1, x+hx);
	    
	    int added=0;
	    // Push values inside the window into vector.
	    // Improvemnt Idea: DRY = do not repeat yourself.
	    for(int y_step = y_min; y_step <= y_max; ++y_step)
	      {
		for(int x_step = x_min, other = nx * y_step; x_step <= x_max; ++x_step)
		  {
		    v[added] = in[x_step + other];
		    added++;
		  }
	      }
	    
	    middle = added>>1;
	    nth_element(v,v+middle,v+added);
	    
	    //static float median1; // Why static make slower.
	    float median1 = v[middle];
	    //cout<< "mediaani" << median1 << endl;
	    // even or odd.
	    // TODO: Improvement. Use median heap
	    switch(added % 2)
	      {
	      case 0:
		out[x + nx*y] = (median1 + *(max_element(v, v + middle)))*0.5;
		//out[x + nx*y] = (median1 + *(max_element(v.begin(), v.begin() + middle)))*0.5;
		break;
	      default :
		out[x + nx*y] = median1;
		break;
	      }
	  }
      }
    free(v);
  }
}
