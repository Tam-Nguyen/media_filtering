#!/usr/bin/octave
function compare_filters()
  %noiserate = 0.4;
  %filenames = {"colorpapers.jpg", "fruits.jpg"};

  different_filters("colorpapers.jpg");
  different_window_sizes("fruits.jpg");
  different_noises("fruits.jpg");
endfunction

function basepath = get_basepath(image)
  [DIR, NAME, EXT, VER] = fileparts (image);
  basepath = "dummy";
  if (length(DIR)>0)
    basepath = strcat(DIR,filesep,NAME);
  else
    basepath = strcat(NAME);
  endif
endfunction


function different_noises(image)
  basepath = get_basepath(image);
  errorfile = strcat("different_noises_",basepath,".txt");
  errorfileid = fopen(errorfile,"w");
  originals = imread(image);
  unbiased = [double(originals(:,:,1)), double(originals(:,:,2)), double(originals(:,:,3))];
  
  noiserate = 0.4;
  spnoises = imnoise(originals, "salt & pepper", noiserate);
  sperrors = calculate_errors(unbiased,spnoises);
  fputs(errorfileid, "Salt and Pepper Noise error:\n");
  fputs(errorfileid, mat2str(sperrors));
  spnoiseR = spnoises(:,:,1);
  spnoiseG = spnoises(:,:,2);
  spnoiseB = spnoises(:,:,3);

  window_size = 5;
  spmedianR = medfilt2(spnoiseR,window_size);
  spmedianG = medfilt2(spnoiseG,window_size);
  spmedianB = medfilt2(spnoiseB,window_size);
  spmedians = cat(3,spmedianR,spmedianG,spmedianB);
  mederrors = calculate_errors(unbiased,spmedians);
  fputs(errorfileid, "\n\nMedian Filter Errors\n");
  fputs(errorfileid, mat2str(mederrors));

  MEAN = 0;
  V = 0.1;
  gnoises = imnoise(originals, 'gaussian', MEAN, V);
  gerrors = calculate_errors(unbiased,gnoises);
  fprintf(errorfileid,"\n\nGaussian noise M=%d, V=%.2f\n",MEAN,V);
  fputs(errorfileid, mat2str(sperrors));
  gnoiseR = gnoises(:,:,1);
  gnoiseG = gnoises(:,:,2);
  gnoiseB = gnoises(:,:,3);

  gmedianR = medfilt2(gnoiseR,window_size);
  gmedianG = medfilt2(gnoiseG,window_size);
  gmedianB = medfilt2(gnoiseB,window_size);
  gmedians = cat(3,gmedianR,gmedianG,gmedianB);
  mederrors = calculate_errors(unbiased,gmedians);
  fputs(errorfileid, "\n\nGaussian Noises Errors\n");
  fputs(errorfileid, mat2str(mederrors));
  
  %imwrite(originals, [basepath ".png"]);

  pnoises = imnoise(originals, 'poisson');
  perrors = calculate_errors(unbiased,pnoises);
  fprintf(errorfileid,"\n\nPoisson noises\n");
  fputs(errorfileid, mat2str(perrors));
  pnoiseR = pnoises(:,:,1);
  pnoiseG = pnoises(:,:,2);
  pnoiseB = pnoises(:,:,3);

  pmedianR = medfilt2(pnoiseR,window_size);
  pmedianG = medfilt2(pnoiseG,window_size);
  pmedianB = medfilt2(pnoiseB,window_size);
  pmedians = cat(3,pmedianR,pmedianG,pmedianB);
  mederrors = calculate_errors(unbiased,pmedians);
  fputs(errorfileid, "\n\nPoisson Noises Errors\n");
  fputs(errorfileid, mat2str(mederrors));


  v=0.4;
  snoises = imnoise(originals, 'speckle', v);
  serrors = calculate_errors(unbiased, snoises);
  fprintf(errorfileid,"\n\n Speckle noises\n");
  fputs(errorfileid, mat2str(serrors));
  snoiseR = snoises(:,:,1);
  snoiseG = snoises(:,:,2);
  snoiseB = snoises(:,:,3);

  smedianR = medfilt2(snoiseR, window_size);
  smedianG = medfilt2(snoiseG, window_size);
  smedianB = medfilt2(snoiseB, window_size);
  smedians = cat(3,smedianR,smedianG,smedianB);
  mederrors = calculate_errors(unbiased, smedians);
  fputs(errorfileid, "\n\nSpeckle Noises Errors\n");
  fputs(errorfileid, mat2str(mederrors));
  
  imwrite(spnoises, [basepath "_sp_diff_noise.png"]);
  imwrite(spmedians, [basepath "_sp_diff_noise_med.png"]);
  imwrite(gnoises, [basepath "_gaus.png"]);
  imwrite(gmedians, [basepath "_gaus_med.png"]);
  imwrite(gnoises, [basepath "_poisson.png"]);
  imwrite(gmedians, [basepath "_poisson_med.png"]);
  imwrite(gnoises, [basepath "_speckle.png"]);
  imwrite(gmedians, [basepath "_speckle_med.png"]);
  
  fclose(errorfileid);
endfunction


function different_filters(image)
  basepath = get_basepath(image);
  errorfile = strcat("different_filters_",basepath,".txt");
  errorfileid = fopen(errorfile,"w");
  originals = imread(image);
  unbiased = [double(originals(:,:,1)), double(originals(:,:,2)), double(originals(:,:,3))];
  
  noiserate = 0.4;
  spnoises = imnoise(originals, "salt & pepper", noiserate);
  sperrors = calculate_errors(unbiased,spnoises);
  fputs(errorfileid, "Salt and Pepper Noise error:\n");
  fputs(errorfileid, mat2str(sperrors));
  spnoiseR = spnoises(:,:,1);
  spnoiseG = spnoises(:,:,2);
  spnoiseB = spnoises(:,:,3);

  window_size = 5;
  spmedianR = medfilt2(spnoiseR,window_size);
  spmedianG = medfilt2(spnoiseG,window_size);
  spmedianB = medfilt2(spnoiseB,window_size);
  spmedians = cat(3,spmedianR,spmedianG,spmedianB);
  mederrors = calculate_errors(unbiased,spmedians);
  fputs(errorfileid, "\n\nMedian Filter Errors\n");
  fputs(errorfileid, mat2str(mederrors));
  
  avgfilter = fspecial("average", window_size);
  spavgR =  imfilter(spnoiseR,avgfilter);
  spavgG = imfilter(spnoiseG,avgfilter);
  spavgB = imfilter(spnoiseB,avgfilter);
  spavgs = cat(3,spavgR,spavgG,spavgB);
  avgerrors = calculate_errors(unbiased,spavgs);
  fputs(errorfileid, "\n\n\Average Filter Errors\n");
  fputs(errorfileid, mat2str(avgerrors));

  %imwrite(originals, [basepath ".png"]);
  imwrite(spnoises, [basepath "_sp.png"]);
  imwrite(spmedians, [basepath "_med.png"]);
  imwrite(spavgs, [basepath "_avg.png"]);
  fclose(errorfileid);
endfunction

function different_window_sizes(image)
  basepath = get_basepath(image);
  errorfile = strcat("different_windows_",basepath,".txt");
  errorfileid = fopen(errorfile,"w");
  originals = imread(image);
  unbiased = [double(originals(:,:,1)), double(originals(:,:,2)), double(originals(:,:,3))];
  
  noiserate = 0.4;
  spnoises = imnoise(originals, "salt & pepper", noiserate);
  sperrors = calculate_errors(unbiased,spnoises);
  fputs(errorfileid, "Salt and Pepper Noise error:\n");
  fputs(errorfileid, mat2str(sperrors));
  spnoiseR = spnoises(:,:,1);
  spnoiseG = spnoises(:,:,2);
  spnoiseB = spnoises(:,:,3);
  imwrite(spnoises, [basepath "_sp.png"]);
				%for window_size = [3 5 7 9 30 33]
  for window_size = [3 5 7 9 30 33]
    %window_size = 3;
    spmedianR = medfilt2(spnoiseR,[window_size window_size]);
    spmedianG = medfilt2(spnoiseG,[window_size window_size]);
    spmedianB = medfilt2(spnoiseB,[window_size window_size]);
    spmedians = cat(3,spmedianR,spmedianG,spmedianB);
    mederrors = calculate_errors(unbiased,spmedians);
    fprintf(errorfileid,"\n\nMedian Filter Errors %dx%d\n",window_size,window_size);
    %fputs(errorfileid, "\n\nMedian Filter Errors\n");
    fputs(errorfileid, mat2str(mederrors));
  %avgfilter = fspecial("average", window_size);
  %spavgR =  imfilter(spnoiseR,avgfilter);
  %spavgG = imfilter(spnoiseG,avgfilter);
  %spavgB = imfilter(spnoiseB,avgfilter);
  %spavgs = cat(3,spavgR,spavgG,spavgB);
  %avgerrors = calculate_errors(unbiased,spavgs);
  %fputs(errorfileid, "\n\n\Average Filter Errors\n");
  %fputs(errorfileid, mat2str(avgerrors));

  %imwrite(originals, [basepath ".png"]);
  imwrite(spmedians, [basepath "_" int2str(window_size) "x" int2str(window_size) "_med.png"]);  
  end  
  %create_pdf("colorpapers.jpg",0.4);
  %create_pdf("fruits.jpg",0.0);
  %clf reset;
  fclose(errorfileid);
endfunction


function different_sp_noise_rates(image)
  basepath = get_basepath(image);
  errorfile = strcat("different_noises_",basepath,".txt");
  errorfileid = fopen(errorfile,"w");
  originals = imread(image);
  unbiased = [double(originals(:,:,1)), double(originals(:,:,2)), double(originals(:,:,3))];

  window_size = 5;
  for noiserate = 0.3:0.2:0.9
    spnoises = imnoise(originals, "salt & pepper", noiserate);
    sperrors = calculate_errors(unbiased,spnoises);
    fprintf(errorfileid,"\nSalt and Pepper Noise Error %d:\n",int32(noiserate*100));
    %fputs(errorfileid, "Salt and Pepper Noise error:\n");
    fputs(errorfileid, mat2str(sperrors));
    spnoiseR = spnoises(:,:,1);
    spnoiseG = spnoises(:,:,2);
    spnoiseB = spnoises(:,:,3);

    spmedianR = medfilt2(spnoiseR,window_size);
    spmedianG = medfilt2(spnoiseG,window_size);
    spmedianB = medfilt2(spnoiseB,window_size);
    spmedians = cat(3,spmedianR,spmedianG,spmedianB);
    mederrors = calculate_errors(unbiased,spmedians);
    fputs(errorfileid, "\n\nMedian Filter Errors\n");
    fputs(errorfileid, mat2str(mederrors));
    
    imwrite(spnoises, [basepath "_sp_rates.png"]);
    imwrite(spmedians, [basepath "_med.png"]);
  end
  fclose(errorfileid);
endfunction


function create_pdf(image,noiserate)
  %errorfile = sprintf("%s_errors.txt",image);
  errorfile = strcat(basepath,"_errors.txt");
  errorfileid = fopen(errorfile,"w");
  
  originals = imread(image);
  unbiased = [double(originals(:,:,1)), double(originals(:,:,2)), double(originals(:,:,3))];
  
  spnoises = imnoise(originals,"salt & pepper",noiserate);
  sperrors = calculate_errors(unbiased,spnoises);
  fputs(errorfileid, "Salt and Pepper Noise error:\n");
  fputs(errorfileid, mat2str(sperrors));
  
  spnoiseR = spnoises(:,:,1);
  spnoiseG = spnoises(:,:,2);
  spnoiseB = spnoises(:,:,3);

     % medfilt2 does not do double conversion.
  spmedianR = medfilt2(spnoiseR);
  spmedianG = medfilt2(spnoiseG);
  spmedianB = medfilt2(spnoiseB);
  spmedians = cat(3,spmedianR,spmedianG,spmedianB);
  mederrors = calculate_errors(unbiased,spmedians);
  fputs(errorfileid, "\n\nMedian Filter Error\n");
  fputs(errorfileid, mat2str(mederrors));
  %save errors.txt colorpapernoiseerrors;
  %save -append errors.txt colorpapermedianerrors;
  %3x3 moving average filter window
  avgfilter = fspecial("average", 3);
  spavgR =  imfilter(spnoiseR,avgfilter);
  spavgG = imfilter(spnoiseG,avgfilter);
  spavgB = imfilter(spnoiseB,avgfilter);
  spavgs = cat(3,spavgR,spavgG,spavgB);
  avgerrors = calculate_errors(unbiased,spavgs);
  fputs(errorfileid, "\n\n\Average Filter Error\n");
  fputs(errorfileid, mat2str(avgerrors));

  subplot(2,2,1,"align");
  imshow(originals);
  title (sprintf ("(a) Original image"));
  subplot(2,2,2,"align");
  imshow(spnoises);
  
  title (sprintf ("(b) %d %% Salt and pepper noise.", noiserate));
  subplot(2,2,3,"align");
  imshow(spmedians);
  title (sprintf ("(c) 3x3 Standard median filter"));
  subplot(2,2,4,"align");
  imshow(spavgs);
  title (sprintf ("(d) 3x3 Moving average filter"));
  
  pdfoutput = strcat(basepath,".pdf");
  print("-dpdf", pdfoutput);
  fclose(errorfileid);
endfunction

function errors = calculate_errors(unbiased, filtered)
    errorR = (unbiased(1)-filtered(:,:,1)).^2;
    errorG = (unbiased(2)-filtered(:,:,2)).^2;
    errorB = (unbiased(3)-filtered(:,:,3)).^2;
    errors = [sum(errorR(:)), sum(errorG(:)), sum(errorB(:))];
endfunction

compare_filters();
