#!/usr/bin/python3
from subprocess import call
from glob import glob
def main():
    #http://sana.aalto.fi/awe/style/vocabulary/index.html
    #http://sana.aalto.fi/awe/style/vocabulary/verbs/index.html
    #VOCABULARY SHIFT, AVOID PHRASAL VERBS
    verbs = ["be", "do", "get", "give", "go", "be going to", "happen", "have", "have to", "have got to", "make", "mean", "put", "look at", "find out", "bring up", "build up", "come up", "do away", "find out", "get rid", "go down", "go up", "take care"]
    #http://sana.aalto.fi/awe/style/vocabulary/adjectives/index.html
    #VOCABULARY SHIFT,  AVOID COLORFUL LANGUAGE
    adjectives = ["a lot", "lots", "big", "small", "tiny", "hard", "good", "bad", "this kind", "this sort", "these kinds", "different" ]
    #http://sana.aalto.fi/awe/style/vocabulary/adverbs/index.html
    # VOCABULARY SHIFT,  ADVERBIAL CONNECTORS 
    adverb = ["often", "sometimes", "nowadays", "a little bit", "a lot", "more and more", "fast"]

    #http://sana.aalto.fi/awe/style/vocabulary/nouns/index.html
    #VOCABULARY SHIFT
    nouns = ["know-how", "trade-off", "trouble", "a way", "idea", "meaning", "answer", "thing", "stuff"]

    #http://sana.aalto.fi/awe/style/vocabulary/prepositions/index.html
    #VOCABULARY SHIFT: Prepositions
    prepositions = ["about", "after", "before", "in", "on top of", "during"]
    everything = "|".join(verbs+adjectives+adverb+nouns)
    call(["egrep",everything,"-w","-i","-o"] + glob("*.tex"))
    #call(["grep","trouble","-w", "-i"] + glob("*.tex"))
main()
